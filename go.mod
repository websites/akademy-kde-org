module akademy-kde-org

go 1.15

require (
	github.com/PhuNH/hg-companion v0.0.0-20230913110054-b505a89c6dad // indirect
	invent.kde.org/websites/hugo-kde v0.0.0-20250119182848-ee83bd1fc3e1 // indirect
)
