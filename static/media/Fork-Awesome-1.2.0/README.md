# Fork Awesome

Modified from Fork Awesome release 1.2.0 to only use the svg files
needed on the site. The envelope-square.svg icon has been modified
to remove the border and enlargen the envelope.  Fork Awesome is
developed at:
https://github.com/ForkAwesome
The main website is:
https://forkaweso.me
