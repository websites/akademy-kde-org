---
title: Akademy 2025 Call for Hosts
SPDX-FileCopyrightText: Akademy Team
author: Akademy Team
SPDX-License-Identifier: CC-BY-SA-4.0
date: 2024-10-21T07:55:49.020Z
categories:
  - Akademy2025
---
If you want to contribute to KDE in a significant way (beyond coding), here is your opportunity — help us organize [Akademy](https://akademy.kde.org) 2025!

We are seeking hosts for Akademy 2025, which will occur in June, July, August, or September. This is your chance to bring KDE’s biggest event to your city! [Download the Call](https://ev.kde.org/akademy/CallforHosts_2025.pdf) for Hosts guide and submit your proposal to akademy-proposals@kde.org by December 1, 2024.

Feel free to reach out with any questions or concerns! We are here to help you organise a successful event and are here to offer you any advice, guidance, or help you need. Let’s work together to make Akademy 2025 an event to remember.