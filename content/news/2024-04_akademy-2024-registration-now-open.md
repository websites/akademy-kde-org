---
title: "Akademy 2024: Registration Now Open"
SPDX-FileCopyrightText: Akademy Team
SPDX-License-Identifier: CC-BY-SA-4.0
date: 2024-04-16T06:12:13.401Z
categories:
  - Akademy2024
---
**Akademy 2024 will be a hybrid event held simultaneously in Würzburg, Germany, and Online.**

Hundreds of participants from the global KDE community, the wider free and open source software community, local organisations and software companies will gather at this year's Akademy 2024 conference. The event will take place in Würzburg and Online from Saturday 7th September to Thursday 12th September.

KDE developers, artists, designers, translators, users, writers, sponsors and supporters from around the world will meet face-to-face to discuss key technology issues, explore new ideas and strengthen KDE's innovative and dynamic culture.

[Register](https://akademy.kde.org/2024/register) now and join us for engaging talks, workshops, BoFs and coding sessions. Collaborate with your fellow KDE contributors to fix bugs, pioneer new features and immerse yourself in the world of open source.

For more information about the conference, visit the [Akademy 2024](https://akademy.kde.org/2024/) website.