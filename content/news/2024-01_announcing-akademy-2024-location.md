---
title: Announcing Akademy 2024 location
SPDX-FileCopyrightText: Akademy Team
SPDX-License-Identifier: CC-BY-SA-4.0
date: 2024-01-11T07:53:56.286Z
hideSponsors: true
categories:
  - Akademy2024
---
[Akademy 2024 ](/2024) will be taking place in Würzburg at the Julius-Maximilians University from Saturday 7th – Thursday 12th September.
