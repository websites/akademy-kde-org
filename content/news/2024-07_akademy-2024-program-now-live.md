---
title: Akademy 2024 Program Now Live
SPDX-FileCopyrightText: Akademy Team
SPDX-License-Identifier: CC-BY-SA-4.0
date: 2024-07-17T08:41:54Z
categories:
  - Akademy2024
---
The [Akademy 2024 Program](https://akademy.kde.org/2024/program/) is now available.

This year's Akademy will take place in Würzburg, a beautiful city where you can enjoy interesting and fascinating talks, panels and keynotes. And for those who prefer to participate remotely, Akademy will also be available online.

Akademy officially kicks off with a welcome event on Friday 6 September, followed by a series of talks on Saturday 7 September and Sunday 8 September. From Monday 9 to Thursday 12 September, there will be BoFs (Birds of a Feather), workshops, meetings, [daytrip](https://akademy.kde.org/2024/daytrip/) and training sessions.

The talks will cover KDE's goals, how we're doing with implementing other languages to code for KDE (Rust anyone?), what's new in the latest wave of desktop and mobile applications, how KDE Eco is saving the environment, backends, frontends, KDE for work, life and fun.

For example, Nicolas Fella will tell us what a software maintainer does and why they are crucial to a project's survival, Aleix Pol Gonzalez will demystify embedded Linux, and Kevin Ottens will take us deep into the core of KDE Neon. You will also learn more about Plasma Mobile, funding your dream project and cool new KWin effects.

You can expect much, much more from a schedule packed with exciting talks and eye-opening presentations. Just take a look at the [full program](https://akademy.kde.org/2024/program/) to discover everything that will be happening.

And that is not all! Stay tuned for the announcement of our two keynote speakers, coming soon here on Planet.

During the week KDE community members will attend BoFs and meet with colleagues with similar interests to work on their projects. They will also attend workshops, meetings, training sessions and daytrip until the event closes on 12 September.