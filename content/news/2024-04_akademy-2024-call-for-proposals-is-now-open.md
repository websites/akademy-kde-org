---
title: Akademy 2024 Call for Proposals is Now Open
SPDX-FileCopyrightText: Akademy Team
SPDX-License-Identifier: CC-BY-SA-4.0
date: 2024-04-10T07:04:55.331Z
categories:
  - Akademy2024
---
[Akademy 2024](https://akademy.kde.org/2024/) will be a hybrid event held simultaneously in Würzburg, Germany, and online. The **[Call for Participation](https://akademy.kde.org/2024/cfp/)** is open! Send us your talk ideas and abstracts.

### Why talk at #Akademy2024

Akademy attracts artists, designers, developers, translators, users, writers, companies, public institutions and many other KDE friends and contributors. We celebrate the achievements and help determine the direction for the next year. We all meet together to discuss and plan the future of the Community and the technology we build. You will meet people who are receptive to your ideas and can help you with their skills and experience. You will get an opportunity to present your application, share ideas and best practices, or gain new contributors. These sessions offer the opportunity for gaining support, and making your plans for your project become a reality.

### How to get started

Do not worry about details or slides right now. Just think of an idea and submit some basic details about your talk. You can edit your abstract after the initial submission. All topics relevant to the KDE Community are welcome. Here are a few ideas to get you started on your proposal:

* New people and organisations that are discovering KDE,
* Work towards *[KDE's goals](https://kde.org/goals/ "https\://kde.org/goals/")*: KDE For All, Sustainable Software, and Automate And Systematize Internal Processes,
* Giving people more digital freedom and autonomy with KDE,
* New technological developments,
* Guides on how to participate for new users, intermediates and experts,
* What's new after porting KDE Frameworks, Plasma and applications to Qt6
* Anything else that might interest the audience?

To get an idea of talks that were accepted, check out the program from previous years: ***[2023](https://conf.kde.org/event/5/contributions/ "https\://conf.kde.org/event/5/contributions/")***, *[2022](https://conf.kde.org/event/4/timetable/ "https\://conf.kde.org/event/4/timetable/")*, *[2021](https://conf.kde.org/event/1/timetable/#20210619 "https\://conf.kde.org/event/1/timetable/#20210619")*, *[2020](https://conf.kde.org/en/akademy2020/public/events "https\://conf.kde.org/en/akademy2020/public/events")*, *[2019](https://conf.kde.org/en/akademy2019/public/events "https\://conf.kde.org/en/akademy2019/public/events")*, *[2018](https://conf.kde.org/en/akademy2018/public/events "https\://conf.kde.org/en/akademy2018/public/events")*, and *[2017](https://conf.kde.org/en/akademy2017/public/events "https\://conf.kde.org/en/akademy2017/public/events")*.

For more details and information, visit our [Call for Participation](https://akademy.kde.org/2024/cfp/).