---
title: Akademy 2024 T-shirt online orders now open & in-person extended
SPDX-FileCopyrightText: Akademy Team
SPDX-License-Identifier: CC-BY-SA-4.0
date: 2024-08-06T06:15:49.020Z
categories:
  - Akademy2024
---
Orders for the Akademy 2024 T-shirt for those attending online are now open till 29th September, these will be shipped after Akademy. For those attending in person the order deadline has been extended till Sunday 11th

Full details are on the [Akademy 2024 T-shirt page](https://akademy.kde.org/2024/tshirt/)

<center>
<a href="/media/2024/akademy2024-mock-attendees-med.png">
 <img src="/media/2024/akademy2024-mock-attendees-med.png" style="width:100%"/>
</a>
<figcaption><center>
Mockup of Akademy 2024 T-shirt by Jens Reuterberg
&nbsp;
</center></figcaption>
</center>
