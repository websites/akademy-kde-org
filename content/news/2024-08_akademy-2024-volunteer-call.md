---
title: Akademy 2024 Call for Volunteers
SPDX-FileCopyrightText: Akademy Team
SPDX-License-Identifier: CC-BY-SA-4.0
date: 2024-08-08T09:15:49.020Z
categories:
  - Akademy2024
---

Akademy needs you! Volunteering is a great way to make new friends and Akademy wouldn't be possible without us all pitching in to make it happen. Find a task or two that sounds fun and sign yourself up! All you need to do is add yourself to a timeslot on the [wiki page](https://community.kde.org/Akademy/2024/Volunteer)
