---
title: Akademy 2024 - The Akademy of Many Changes
SPDX-FileCopyrightText: Akademy Team
SPDX-License-Identifier: CC-BY-SA-4.0
date: 2024-09-12
categories:
  - Akademy2024
---

<center>
<a href="/media/2024/akademy2024-groupphoto_crop_1500.jpg">
 <img src="/media/2024/akademy2024-groupphoto_crop_1500.jpg"
style="width:100%"/>
</a>
<figcaption><center>
Akademy 2024 group photo.
&nbsp;
</center></figcaption>
</center>

This year's Akademy in Würzburg, Germany was all about resetting priorities,
refocusing goals, and combining individual projects into a final result
greater than the sum of its parts.

A shift — or more accurately a broadening of interest — in the KDE community has
been gradually emerging over the past few years, and reached a new peak at this
year's event. The conference largely focused on delivering high quality,
cutting-edge Free Software to end users. However the keynote "Only Hackers will
Survive" by tech activist and environmentalist Joanna Murzyn, and Joseph De
Veaugh-Geiss' "Opt In? Opt Out? Opt Green!" talk took attendees down a left turn
by addressing growing concerns about the impact of IT on the environment and
discussed how Free Software can help curb CO2 emissions.

<center>
<a href="/media/2024/joanna.jpg">
 <img src="/media/2024/joanna.jpg"
style="width:100%"/>
</a>
<figcaption><center>
Joanna Murzyn explains the impact irresponsible IT developments have on the
environment.
&nbsp;
</center></figcaption>
</center>

KDE has always had a social conscience. The community's vision and mission of
providing users with the tools to control their digital lives and protect their
privacy is now combined with concern for the preservation of the planet we live
on.

But KDE can do more than one thing at a time, and our software is also
undergoing profound changes under the hood. Joshua Goins, for example, is
working on ways to enable framework, plasma and application development in Rust.
According to Joshua, Rust has many advantages, such as its memory safety
capabilities, while adding a Qt front-end to Rust projects is much easier than
many people believe.

This is in line with one of the new goals adopted by the community during this
Akademy: Nicolas Fella, Plasma contributor and key developer of KDE's
all-important frameworks, will champion "Streamlined Application Development
Experience", a goal aimed at making it easier for new developers to access KDE
technologies.

And onboarding new developers is what the "KDE Needs You! 🫵" goal is about.
While the growing popularity of KDE software is great, growth puts more stress
on the teams that create the applications, environments, and underlying engines
and frameworks. Add to that the fact that the veteran developers are getting
gray around the temples, and you need a constant influx of new contributors to
keep the community and its projects running. The "KDE Needs You! 🫵" goal aims
to address this challenge and is being spearheaded by members of the Promo and
Mentoring teams. The champions aim to formalize and strengthen KDE's processes
for recruiting active contributors, and to make recruiting active contributors
to projects a priority and an ongoing task for the community.

These two goals aim to benefit the community and projects in general. KDE's
third goal, "We care about your input", will build on their work: proposed by
Jakob Petsovits, Gernot Schiller and Joshua Goins, "input" in this case refers
to "input from devices". The goal addresses the fact that there are still a lot
of languages and hardware that are not optimally supported by Plasma and KDE
applications, and with the move to Wayland, some devices have even temporarily
lost a measure of support they enjoyed on X11. Jakob, Gernot, and their team of
supporters plan to solve this problem methodically, working to make KDE's
software work smoothly and effortlessly on drawing tablets, accessibility
devices, and game controllers, as well as software virtual keyboards and input
methods for users of the Chinese, Japanese, and Korean languages.

One way to achieve the desired level of integration is to control the entire
software stack, right down to the operating system. This is also in the works
for KDE, as Harald Sitter is working on a new technologically advanced operating
system tentatively named "KDE Linux". This operating system aims to break free
of the constraints currently limiting KDE's existing Neon OS and offer a
superior experience for KDE's developers, enthusiast users, and everyday users.

KDE Linux's base system will be immutable, meaning that nothing will be able to
change critical parts of the system, such as the `/etc`, `/usr`, and `/bin`,
directories. User applications will be installed via self-contained packages,
such as Flatpaks and Snaps. Adventurous users and developers will be able to
overlay anything they want on top of the base system in a non-destructive and
reversible way, without ever having to touch the core and risk
not-easily-fixable breakage.

This will help provide users with a solid, stable, and secure environment,
without sacrificing the ability to run the latest and greatest KDE software.

As the proof of the pudding is in the eating, Harald surprised the audience when
he revealed towards the end of his talk that his entire presentation had been
delivered using KDE Linux!

The user-facing side of KDE is also changing with the work of Arjen Hiemstra and
Andy Betts — and the Visual Design Group at large. Arjen is working on Union, a
new theming engine that will eventually replace the different ways of styling in
KDE. Up until now, developers and designers have had to deal with multiple ways
of doing styling, some of which are quite difficult to use. Union, as the name
implies, will end the fragmentation and provide something that is both easier
for developer to maintain and more flexible for designers to interact with.

And then Andy Betts told us about *Plasma Next*, while introducing the audience
to the concept of *Design Systems* -- management systems that allow all aspects
of large collections of icons, components, and other graphical assets to be
controlled. Using design systems, the VDG is developing a new look for Plasma
and KDE applications that may very well replace KDE's current Breeze theme — and
be consistently applied across all KDE apps and plasma, to boot!

<center>
<a href="/media/2024/dolphin_next.png">
 <img src="/media/2024/dolphin_next.png"
style="width:100%"/>
</a>
<figcaption><center>
A first look at Dolphin rocking Plasma Next icons.
&nbsp;
</center></figcaption>
</center>

This means that in the not too distant future, KDE users will be able to enjoy a
rock-solid and elegant operating system with a brilliant look to match!

## In other Akademy news...

- Kevin Ottens added another KDE success story to the list, telling us how a
wine producing company in Australia has been using hundreds of desktops running
KDE Plasma for more than 10 years.
- Natalie Clarius explained how she is working on adapting Plasma to work better
on our vendor partners' hardware products.
- In the "Openwashing" panel moderated by Markus Felner, Cornelius Schumacher of
KDE, Holger Dyroff of OwnCloud, Richard Heigl of HalloWelt! and Leonhard Kugler
of OpenCode took on companies that call themselves "open source" but are
anything but.
- David Schlanger shared the harsh truth about AI, his wishful positive vision
for the technology, and then faced questions and comments from Lydia Pintscher
and Eike Hein in the keynote session on day 2.
- Ben Cooksley, Volker Kraus, Hannah von Reth, and Julius Künzel took a deep
dive into the frameworks, services, and utilities that help KDE projects get
their products to users quickly and on a wide variety of platforms.

## Akademy Awards

The prestigious KDE Awards were given to:

- Friedrich W.H. Kossebau for his work on the
[Okteta](https://apps.kde.org/okteta/) hex editor.
- Albert Astals Cid for his work on the Qt patch collection, KDE Gear release
maintenance, i18n, and many, many other things.
- Nicolas Fella received his award for his work on KDE Frameworks and Plasma.
- As is traditional, the Akademy organizing team was awarded for putting on a
fun, interesting and safe event for the entire KDE community.

<center>
 <video width="1280" height="720" controls>
  <source
src="https://cdn.kde.org/promo/Akademy2024/akademy_awards_+_orga.webm"
type="video/webm">
  <source src="https://cdn.kde.org/promo/Akademy2024/akademy_awards_+_orga.mp4"
type="video/mp4">
  Your browser does not support the video tag.
 </video>
</center>

If you would like to see the talks as they happened, [the unedited videos are
currently available on YouTube](https://www.youtube.com/@KdeOrg/streams). Cut
and edited versions with slides will be available soon on both YouTube and
PeerTube.
