---
title: Akademy 2024 T-Shirt orders open
SPDX-FileCopyrightText: Akademy Team
SPDX-License-Identifier: CC-BY-SA-4.0
date: 2024-07-12T08:47:06.254Z
categories:
  - Akademy2024
---
Pre-orders are now open for the [Akademy 2024 T-shirt](https://akademy.kde.org/2024/tshirt), this is only for those who will be attending Akademy, in person, in Würzburg

Pre-orders will close on 31st July

We will  be opening in a few weeks separate online orders for t-shirts for those who aren't attending in person