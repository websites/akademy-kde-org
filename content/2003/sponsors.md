---
title: Sponsors
menu:
  "2003":
    weight: 4
hideSponsors: true
---


<a href="www.hp.com"><img class="img-fluid" src="/media/2003/hp.gif"></a>

<a href="http://www.klaralvdalens-datakonsult.se/"><img class="img-fluid"
src="/media/2003/kdablogo-neu_h50.png"></a>

<a href="http://www.suse.com/"><img class="img-fluid"
src="/media/2003/suse.gif"></a>

<a href="http://www.trolltech.com/"><img class="img-fluid"
src="/media/2003/logo_face_trolltech_h70.gif"></a>

## Media partner

<a href="http://www.linuxnewmedia.de/"><img class="img-fluid"
src="/media/2003/Linux_Magazine_h50.jpg"></a>

## Location Sponsor

<img class="img-fluid" src="/media/2003/pecet2barevna_h80.JPG"></a>

<a href="http://www.fhs-hagenberg.ac.at/"><img class="img-fluid" src="/media/2003/FH2_h60.jpg"></a>

## Become a Sponsor



The KDE Developers' Conference is run by KDE e.V., the Academic and University
Center Nové Hrady and the Polytechnic University of Upper Austria in Hagenberg.
Any donations that will help financing this event are more than welcome. 
