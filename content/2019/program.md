---
title: Program Overview
menu:
  "2019":
    weight: 21
    parent: details
---

<table border="1" style="border-top-style: solid; border-top-color: #ccc; border-top-width: 2px;">
<tr>
<th>Day</th>
<th>Morning</th>
<th>Afternoon</th>
<th>Evening</th>
</tr>
<td>Fri 6th</td>
<td colspan="2"> <center></center></td>
<td><a href="/2019/welcome-event-friday">Welcome Event</a></td>

<tr>
<td>Sat 7th</td>
<td colspan="2"><center><a href="https://conf.kde.org/en/akademy2019/public/schedule/1">Talks Day 1</a></center></td>
<td> </td>
</tr>
<tr>
<td>Sun 8th</td>
<td colspan="2"><center><a href="https://conf.kde.org/en/akademy2019/public/schedule/2">Talks Day 2</a></center></td>
<td><center><a href="/2019/social-event-sunday">Social Event</a></center></td>
</tr>
<tr>
<td>Mon 9th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2019/Monday">BoFs, Workshops &amp; Meetings</a><br /><a href="https://ev.kde.org/generalassembly/">KDE eV AGM</a></center></td>
<td> </td>
</tr>
<tr>
<td>Tue 10th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2019/Tuesday">BoFs, Workshops &amp; Meetings</a></center></td>
<td><center><a href="/2019/lgbtq-meal-tuesday">LGBTQ+ Meal</a></center></td>
</tr>
<tr>
<td>Wed 11th</td>
<td><center><a href="https://community.kde.org/Akademy/2019/Wednesday">BoFs, Workshops &amp; Meetings</a></center></td>
<td colspan="2"><center><a href="/2019/daytrip-wednesday">Daytrip</a></center></td>
</tr>
<tr>
<td>Thu 12th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2019/Thursday">BoFs, Workshops &amp; Meetings</a></center></td>
<td> </td>
</tr>
<tr>
<td>Fri 13th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2019/Friday">BoFs, Workshops &amp; Meetings</a><br /><a href="https://community.kde.org/Akademy/2019/TrainingCommunication">Communication Training</a></center></td>
<td> </td>
</tr>
</table>

## Lunch options

Most food places are located inside the "food court", between Piazza della
Scienza and Piazza dela Trivulziana, or in terms of university buildings between
U3/U4 and U12.  You are welcome to choose your own lunch depending on your
tastes and inspiration.

## Note for the weekend

Weekends are tricky for Bicocca, in particular Sunday. The places which are
going to be open are going to be very few and not have enough food to feed
Akademy, as usually not much happens on the weekend in Bicocca.

We made a selection and made sure they will have enough food, and additionally
negotiated special prices for the conference.

Please try to go in one of these places to avoid inconveniences or long waiting
times, especially on Sunday, where even places which are open might not be
staffed for handling hundreds of participants.

- PanCafé - Expect to pay ~6€ for a pizza al trancio + water (final amount
depends on actual pizza weight, you will be able to indicate the amount you
want). For akademy they are throwing in a free coffee/americano/macchiato for
every lunch. They have a direct oven where they bake continuously so this will
be a great pizza.
- Farinami - For 8€ you will be getting the following
combination: 
  - Buffet - Pizza al trancio, focaccia pugliese, warm pasta,
crocchete, zeppole napoletane, bruschettine miste, mini sandwiches, salami,
prosciutto, cheeses, cous cous, farro with tomato and tuna, vitel tonné.
  - A standard drink (water, soda, beer wine)
  - Coffee
- PQuattordici - With 7€-10€ you will do a lunch complete with a panino, a piadina
or a salad, water or soda and coffee

All these places are by the food court on the left (when coming from U4),
roughly in front of Je Suis Jambon (where we had the Welcome event). The first
place, right in front of Jambon is Farinami, going a little forward you will
find PanCafé and on the backside of PanCafé there is PQuattordici.
