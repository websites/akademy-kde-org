---
title: Welcome Event - Friday
menu:
  "2019":
    weight: 24
    parent: details
---

Host: Je Suis Jambon  
Time: 18 - 22:30  
Address: Piazza della Trivulziana (but don't use this address as it's closeby
but not exactly where this pub is).

The welcome event will happen next to Piazza della Scienza, you can pop by as
early as at 18. From there, just cross below the arch made by the U3 and U4
buildings, cross the street and walk about 20 meters. You will see us on your
right, at a gastropub called "Je Suis Jambon". In doubt, just ask, most students
will know where it is and point you to the right direction.

There will be food (piadine, focaccie, pizza), and drinks. You can enjoy a
typical aperitivo, and spend time with your favorite KDE hackers. The place is
very well known for having great cured meat based products (responsibly sourced,
e.g. Slow Food presidia) and a large beer selection from local microbreweries,
with a small selection of guest bottles. While vegan and non-alcoholic options
will always be available, if you are into handcrafted beers... make sure you
taste their selection!

You are going to receive one token for food, and two "meta" tokens which you can
use for additional food or drinks. A registration desk will be available.

