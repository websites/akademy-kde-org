---
title: LGBTQ+ Meal - Tuesday
menu:
  "2019":
    weight: 27
    parent: details
---

Meet, cook & eat together

We are having an event for LGBT & Queer attendees this year on the evening of
Tuesday 10th.

Leaving the venue shortly after the BoF Wrapup session we will make our way to a
nearby apartment and chat while we cook our meal together then eat it

This event is free to attend however so that we can plan for it, if you would
like to attend please email [Kenny D](mailto:kenny@kde.org) or if you have any
questions about this.

Feel free to bring any favourite drinks you have to supplement the limited
selection that will be available with the meal.

