---
title: Akademy 2019 T-shirt
menu:
  "2019":
    weight: 28
    parent: details
---

<div style="float:right; text-align: center; padding: 1ex; margin: 1ex;
width: 300px; border: 1px solid grey;">
<a href="/media/2019/ak2019-attendee-mock.png">
  <img src="/media/2019/ak2019-attendee-mock-wee.png" 
  alt="A white t-shirt with the akademy 2029 design on it">
</a>
<figcaption>
Mockup of Akademy 2019 t-shirt by Jens Reuterberg
</figcaption>
</div>

**Pre-ordering is no longer available. However there will be a small number of
t-shirts available for anyone who has not pre-ordered.**

Following on from previous years we have the option for buying an official
Akademy 2019 t-shirt. Attendees can pre-order a t-shirt, pick it up at Akademy
and pay a reduced price.

It's hard for the team to guess how many of each size shirt to order. Sometimes
people aren't able to get the right size because they have sold out, and we
often end up with leftovers of other sizes.

This year's design was created by Jens Reuterberg and is inspired by the [Milan
Cathedral](https://en.wikipedia.org/wiki/Milan_Cathedral)

~~If you are definitely coming to Akademy 2019, you can pre-order the size of
t-shirt that you want. Then pick it up and pay during the conference.
Pre-ordered t-shirts have a reduced price of 12€, normally 15€.~~

~~To pre-order your t-shirt, click the Pre-Order T-shirt option in your Akademy
registration and select the size you want.  T-shirt pre-orders are open till
Monday 12th Aug at 1100CEST~~

If you don't know whether or not you will be at Akademy, or if you don't want to
pre-order, there will be a limited number of t-shirts available to buy at the
event for 15€.

The fitted style is suitable for some women, while others may prefer to wear the
unisex style.

This years t-shirts are the same model as last years

The approximate sizes of the t-shirts are:

Unisex
![Tshirt with a table of dimensions of different sizes](/media/2019/TU03T.jpg)
*4XL and 5XL are not available in the same colour unfortunately, they will be in
a similar Sports Grey colour*

Fitted
![Tshirt with a table of dimensions of different sizes](/media/2019/TW04T.jpg)
*3XL is not available in the same colour unfortunately,they will be in a similar
Sports Grey colour*

