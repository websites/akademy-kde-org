---
title: Accomodation
menu:
  "2019":
    weight: 32
    parent: travel
---

September is a warm month in Milan, many events happen in the city during this
time and accommodations can be scarce, especially during weekends. Luckily, the
Milano Bicocca University is well-connected. You can stay almost anywhere in the
city and get to the conference within 30 minutes, or less, using public
transport.

We managed to pre-book a number of room across a few facilities to get a better
deal. Pricing varies during the week and weekends (Friday - Saturday nights). We
encourage you to book sooner than later as prices can get higher and options are
limited!

### Hotel Arcimboldi, 4* hotel

Single: € 150,00 (weekend), € 80,00 (week)  
Twin: € 170,00 (weekend), € 100,00 (week)  
**To get the conventioned prices, you need to send an e-mail to
[arcimboldi@bookingsolutions.it](mailto:arcimboldi@bookingsolutions.it),
mentioning the "KDE Akademy" event happening at the "University of Milan Bicocca",
and ask for conventioned prices**

### Ostello Bello Grande, Top Hostel

**This offering has free dinner included!**  
If you book before the 15th of June:  
Single: € 170,00 (weekend), € 75,00 (week)  
Twin: € 250,00 (weekend), € 115,00 (week)  
Website: [https://www.ostellobello.com/hostel/ostello-bello-grande-railway-station...](https://www.ostellobello.com/hostel/ostello-bello-grande-railway-station-milan/)  
Triple (and more) options available for a considerable saving  
**To get the conventioned prices, you need to send an e-mail to
[info.lepetit@ostellobello.com](mailto:info.lepetit@ostellobello.com) mentioning "AKADEMY"**

### New Generation Città Studi, Hostel

Twin: € 46,55 per night  
Website: [https://www.newgenerationhostel.it/](https://www.newgenerationhostel.it/)  
Triple (and more) options available for a considerable saving  
**Coupon for 10% discount: UNIXMIB2019**  

### New Generation Brera, Hostel

Twin: € 46,55 per night  
Website:
[https://www.newgenerationhostel.it/](https://www.newgenerationhostel.it/)  
Triple (and more) options available for a considerable saving  
**Coupon for 10% discount: UNIXMIB2019**

## Alternative accommodation

As many of the above hotels will be sold fast, here are other accommodation
options you can take a look at which are within 10-15 minutes of commute time
from the location.

Note - many of these hotels are fully booked for the weekend, so consider
alternative accommodation for the first two nights:

- [Hilton Garden Inn](https://hiltongardeninn3.hilton.com/en/hotels/italy/hilton-garden-inn-milan-north-MILGIGI/index.html)
- [NH Milano
  Concordia](https://www.nh-hotels.com/hotel/nh-milano-concordia?campid=8435708&gclid=Cj0KCQjwrdjnBRDXARIsAEcE5Yk7ZzdwmznoDtbPjWGWYVUYPOcCKOO6Ika6C_1-trwx8Wh09tIZIIoaAhDcEALw_wcB)
- IBIS Ca' Granda
- [Hotel S. Guido](https://www.hotelsanguido.com/en/index.html)
- [B&B Hotel Milano Sesto](https://www.hotel-bb.com/en/hotels/milano-sesto.htm)
- NOVOTEL Ca' Granda
- [Gogol Ostello](https://www.gogolostellomilano.com/home---eng)
- [Hostel California](http://hostelcaliforniamilano.com/it/index.php)

**Apartments are also available to rent as an alternative to hotel/hostels. Many
have a similar price to the hotel rooms, have a look on booking.com, trivago,
airbnb etc**

