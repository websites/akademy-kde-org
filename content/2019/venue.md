---
title: Venue
menu:
  "2019":
    weight: 22
    parent: details
---

The event will be held in building U4 at the weekend and through out U1-U4
during the week, Piazza della Scienza (45.513661,9.2091579,17), Milan: [[Google
Maps](https://www.google.com/maps/search/piazza+della+scienza/@45.513661,9.2091579,17z)]
or [[OpenStreetMap](https://www.openstreetmap.org/search?query=piazza%20della%20scienza%20milano#map=18/45.51372/9.21157)]

Close [public transport stations](/2019/getting-around-milan) include

- Metro: M5 Bicocca, M1 Precotto
- Suburban trains: (S) Greco Pirelli
- Tram: Line 7, Universita Bicocca - Scienza
- Bus 87 which connects it in about 15mins with Central Station, and you can take
a 5 minutes train from either Lambrate or Garibaldi (two other major train stops)

These lines when combined connect a very large part of the city, which make
Bicocca an easy place to reach from most locations.
