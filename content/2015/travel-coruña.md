---
title: Travel to A Coruña
menu:
  "2015":
    weight: 41
    parent: travel
---

## How to arrive
  - [By Plane](#by-plane)
    - [A Coruña Airport (LCG)](#a-coruña-airport-(lcg))
    - [Santiago de Compostela Airport
      (SCQ)](#santiago-de-compostela-airport-(scq))
    - [Other options](#other-options)
  - [By train](#by-train)
    - [Destinations](#destinations)
    - [Renfe website and mobile app](#renfe-website-and-mobile-app)
    - [Renfe promotions](#renfe-promotions)
  - [On foot or by bike](#on-foot-or-by-bike)

# By plane
## A Coruña Airport (LCG)

A Coruña Airport has frequent connections to Madrid (Iberia, Air Europa) and
Barcelona (Vueling), as well as some flights to other Spanish cities (Bilbao,
Seville, Canary Islands). It also has daily flights to other European hubs, such
as London Heathrow (Vueling, codesharing with IB & BA), and Lisbon (Tap).

You can check all destinations and airlines at [the Wikipedia page about the
airport](https://en.wikipedia.org/wiki/A_Coru%C3%B1a_Airport#Airlines_and_destinations).

When searching for flights, please take into account that this airport might be
appear as either "A Coruña" or "La Coruña".

Once you are at the airport you can go to the city:

* By bus: (Bus line: 4051)
  * Price: 1,5€
  * Airport - A Coruña city center:
    * Mon - Fri: Once every 30 minutes from 7:15 to 21:45
    * Sat - Sun: Once every hour from 9:00 to 22:00
  * A Coruña city center - Airport:
    * Mon - Fri: Once every 30 minutes from 7:15 to 21:45
    * Sat - Sun: Once every hour from 8:30 to 22:30
  * By taxi:
    * Estimate price for city center: 20€
    * Estimate price for Rialta: 10€

There is a left luggage facility at the A Coruña bus station, signposted as
*consigna* open from 08:00 to 22:00

## Santiago de Compostela Airport (SCQ)

In addition to frequent flights to Spanish cities (including Madrid, Barcelona,
Málaga, Mallorca, Bilbao), this airport also has a wide range of European
destinations (including London Stansted & Gatwick, Istambul, Zurich, Geneva,
Paris CDG, Rome FCO, Amsterdam, Brussels, Munich, Berlin TXL).

You can check all the destinations at [the Wikipedia page about the
airport](https://en.wikipedia.org/wiki/Santiago_de_Compostela_Airport#Airlines_and_destinations).

### How to arrive from Santiago Airport to A Coruña
#### Direct bus

There's a direct bus operated by Travidi. Timetable is as follows:

* Weekdays:
  * A Coruña to Santiago Airport: 6:15, 18:45
  * Santiago Airport to A Coruña: 10:00, 22:15
* Saturdays, Sundays and public holidays:
  * A Coruña to Santiago Airport: 18:45
  * Santiago Airport to A Coruña: 21:30
* Aproximate travel time: 1 hour
* Travidi website in some languages has old timetable
* Departure point from A Coruña is Hotel NH Atlántico, and price is 7,55€

#### Bus to Santiago and then a train or bus to A Coruña

There's a bus service available that links Santiago Airport with Santiago, and
it stops both at the bus station and train station of Santiago de Compostela.
You can check the timetable, and even buy the tickets, at 
[Empresa Freire website](http://www.empresafreire.com/html/ingles/seccion3a.php)
(The airport is named "Labacolla airport" in this webpage). Please note that
buses of route "Santiago - Lugo" might not take passengers for the airport.

You can check train timetables at [Renfe](http://renfe.com/) website, however,
we do not recomend to
buy the tickets on advance unless you are sure that you'll arrive on time to the
station, as you'll need to use them on the specific train you bough (So if you
bought one for the 18:00 train that ticket will not be valid for the 19:00
train), we advise to buy the tickets at the station instead. You can use the
Akademy train discount when you buy the Santiago - Coruña ticket. Please have a
look at the ["By train"](#by-train) section of this webpage in order to view more
information.

### Other options

If you don't find any connection to either A Coruña or Santiago, you can also
check [Vigo
Airport](https://en.wikipedia.org/wiki/Vigo_Airport#Airlines_and_destinations)
in the south of Galicia and Porto airport in Portugal.

### Porto airport (OPO)

Porto airport has a larger list of European destinations than any of the
Galician airports. And some intercontinental destination. However, it's located
more than 3 hours by bus from A Coruña, the buses are not very frequent, and
most likely you'll have to change bus or train in either Vigo, Pontevedra or
Santiago. For that reason, we do not recomend this option unless you don't find
a reasonable way to arrive at either A Coruña or Santiago airports.

You can check all the destinations at [the Wikipedia page about the
airport](https://en.wikipedia.org/wiki/Francisco_de_S%C3%A1_Carneiro_Airport#Airlines_and_destinations).

#### How to arrive from Porto Airport to A Coruña

Please note that TAP airlines offers a complimentary bus to Galicia for people
arriving to Porto airport on some TAP flights. [More information available at TAP
webpage](http://www.flytap.com/Espana/en/Promotions/OffersPortugal/connection-porto-vigo).
Other options are as follows:

* [Alsa](http://alsa.es/) has a daily bus from Porto Airport to A Coruña.
* [Autna](http://www.autna.com/es/horarios-y-tarifas/) has various daily buses
  from Porto Airport to Vigo or Santiago, where there are frequent buses and
  trains to A Coruña. 

Please take into account that: Portugal time-zone is WEST (UTC +1), while Spain
time-zone is CEST (UTC +2) and that Porto airport might either appear as "Porto
airport" or "Oporto airport"

# By train

A Coruña has a train station served by Renfe.

## Destinations

There are frequent trains to Galician cities: Santiago de Compostela,
Pontevedra, Vigo, Lugo, Ourense. In the case of the trains to Santiago,
Pontevedra and Vigo the train is very fast (40 minutes to Santiago, 2 hours to
Vigo) and frequent (each 40 minutes).  There are also daily trains to Madrid (it
takes 6 hours, or 9 hours if you take the night train), Barcelona (it's a night
train, it takes 9 hours), Irun (it takes 11 hours).  Each of those trains stop
in various cities.

## Renfe website and mobile app
### How to set language in Renfe app

If you have your smartphone configured in a language different than either
English or Spanish, the Renfe app will only display strange captions instead of
the correct text. In order to correct so:

1. Start Renfe app (It might appear as app_name)
2. In the main menu select "menuConfig"
3. If a popup appears, select "alertAccept"
4. Click "lang_select"
5. Choose "lang_esp" if you want to use the application in Spanish and
"lang_eng" if you want to use the application in English.

### How to change language of Renfe webpage

If you change the language in Renfe homepage, it will not affect the ticket
selling section. In order to view the ticket buying interface in
English/French/Galician/Catalan/Vasque, you need to enter directly in
https://venta.renfe.com/vol/inicioCompra.do and then select your language (You
click the "Welcome" text in the language you prefer, in the upper right area).
Some texts describing some conditions of the fares are only still available in
Spanish.

## About Renfe promotions

For each train various fares might appear. If hover the icon near the price, it
will display the conditions of that particular fare. The conditions are
displayed in Spanish, "Cambio" and "Anulaciones" specify the percentage of the
price you will have to pay as a fee if you decide to change your ticket, or to
ask for a refund. If "No" is specified following "Cambio" or "Anulaciones", it
means that this fare does not allow changes or refunds. Apart from the fares
displayed, Akademy attendees do also have available an special fare.  Apart of
the fares, there might be two kind of services: Turista and Preferente.
Preferente is a superior class than Turista and the price is usually higher,
however, depending on the available fares, sometimes it might be cheaper than
Turista.  If you are booking a night train, if the class specifies "Cama", then
you will travel in a bed, if not, you will travel in an armchair.

### Akademy fare

Akademy attendees do have a special fare available. The Akademy fare has the
following conditions:

* The ticket price will be a 30% reduction over the nominal price (the one which
  is usually close to the "F" symbol)
* Ticket are exchangeable.
* Tickets can be refunded, but a 5% of the payed ammount will not be refunded.

In order to buy the ticket with the Akademy fare at Renfe website, please follow
[the following instructions at RENFE
webpage](http://www.renfe.com/EN/viajeros/tarifas/eventos_tutorial.html). You'll
need to download the [discount voucher](/media/2015/TES_2015_00864.pdf), print it
and take it with you alongside the ticket when you board the train.

### Mesa fare

When searching the train fares, you will sometime see offers with very good
price, marked with a "4M" symbol. Those are called "Tarifa mesa". Take into
account that those fares are for a pack of four tickets. That means, that even
if you are making the trip alone, with one friend, with two friends or with
three friends, if you select that fare you will have to pay four tickets. In the
same manner, if you are a group of five you will have to buy eight tickets.

# On foot or by bike

The Way of Saint James has been the preferred way to reach Santiago de
Compostela from any point of Europe for centuries. If you want to come to
Akademy on foot or by bike, it might be a good idea to do the Way of Saint James
and once in Santiago take a train to Coruña.

In order to do so, you have to go to one of the points of the itinerary and
begin walking. Follow all the indications and the yellow arrows. In a few
days/weeks you'll arrive at Santiago.

