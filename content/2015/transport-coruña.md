---
title: Transport in A Coruña
menu:
  "2015":
    weight: 34
    parent: travel
---

## Local buses

![Linea E Especial Universidad](/media/2015/scale.jpg)

Public transport in A Coruña is provided by [Tranvías
Coruña](http://www.tranviascoruna.com/), with 23 lines during the day. If you
want to go somewhere at night, you only have one bus line (BUHO) on friday and
saturday nights, every hour from city center.

Single ticket bus price is 1,30€. Tickets are bought in the bus, in order to buy
them coins and 5€ notes are accepted, but not bigger notes.

You can download their APP which gives you buses timetable and the time you will
have to wait in a stop for the next bus in real time:

- Android
  - [Official
    application](https://play.google.com/store/apps/details?id=itranvias.sistemasolton&hl=es)
  - [Unofficial
    application](https://play.google.com/store/apps/details?id=eu.barbeito.buscoruna)
- [Apple Store](https://itunes.apple.com/app/id876315471)
- [HTML5 App](http://www.itranvias.com/)

The official applications require internet connection availability in order to
work, while the unofficial one provides some functionality even offline.

Please note that in order to reach Akademy venue, the recommended bus line is
"Especial Universidad", which can be identified by the letter "E" or "UDC"
(Depending on the bus, but the line is the same). (See photo)

On Saturday 25th, and on Sunday, "Especial Universidad" line is not available,
so you'll have to use either line 5 (Espacio Coruña stop), or line 24 (Antonio
Insua 34 stop), or line 22 (A. Molina Campus Elviña stop).

## Rialta bus services

![Rialta bus](/media/2015/rialta-portada3.jpg)

People staying at Rialta you can use their free bus services from Rialta to
Coruña city center, stopping at the Akademy venue. Please check [Rialta buses
timetable](/media/2015/Bus%20Rialta.pdf)
 
## Taxis

<!-- Original image of taxi stop is no longer available, previous url
http://fotos02.laopinioncoruna.es/fotos/noticias/318x200/2012-02-26_IMG_2012-02-19_01.55.52__6775861.jpg
-->

If you need a fast transport, you are trying to return home at night or you
don't have any bus line to go your destination, you can call a taxi using the
phone number (+34) 981 28 77 77 or if you prefer, you can use some oficial APPs
too:

- TaxiClick
  - Available in Spanish, German, Catalan, Croatian, French, English, Italian,
    Portuguese, Swedish and Turkish.
  - [Google
    Play](https://play.google.com/store/apps/details?id=com.interfacom.taxiclick)
  - [Apple Store](https://itunes.apple.com/es/app/taxiclick/id510296062?mt=8)
- PideTaxi
  - Available in English and Spanish
  - [Google
    Play](https://play.google.com/store/apps/details?id=es.sooft.pidetaxi&hl=es)
  - [Apple
    Store](https://itunes.apple.com/es/app/pidetaxi/id737441460?mt=8&ign-mpt=uo=4)

Approximate taxi price from city center to Rialta or to the Computer Science
Faculty is about 10 €

In the Akademy venue, you can request a taxi at Infodesk. If you're staying at
Rialta, you can ask them to call a taxi in reception; if leaving early, tell
them the day before that you'll need a taxi. If you are staying in a different
accommodation, most likely they will also call a taxi if you ask for it in
reception.

You have a map of the most important designated taxi stops of the city in the
following URL: http://www.paxinasgalegas.es/taxis-a-coru%C3%B1a-487ep_31ay.html
(please click "Agrandar tamaño").

