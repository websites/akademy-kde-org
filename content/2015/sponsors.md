---
title: Sponsors
menu:
  "2015":
    weight: 7
---

## Sponsorship Opportunities

**A big thank you to our sponsors who help to make this event happen!
There are still sponsorship opportunities.  
If you are interested, please see [Sponsoring Akademy 2015](/2015/sponsoring)
for more information, including valuable sponsor benefits.**

## Sponsors for Akademy 2015


### Silver

{{< sponsor homepage="https://developers.google.com/open-source" img="/media/2014/google.png" >}}

<p id="google"><strong>Google</strong> is a proud user and
supporter of open source software and development methodologies. Google
contributes back to the Open Source community in many ways, including more than
50 million lines of source code, programs for students including Google Summer
of Code and the Google Code-in Contest, and support for a wide variety of
projects, LUGs, and events around the world.  Learn more at:</p>

<https://developers.google.com/open-source/>

{{< /sponsor >}}

{{< sponsor homepage="http://www.qt.io" img="/media/2015/theqtcompany_0.png" >}}

<p id="theqtcompany"><strong>The Qt Company</strong>,
a wholly owned subsidiary of Digia Plc., is responsible for all Qt activities
including product development, commercial and open source licensing together
with the Qt Project under the open governance model. Together with our licensing,
support and services capabilities, we operate with the mission to work closely
with developers to ensure that their Qt projects are deployed on time, within
budget and with a competitive advantage. Learn more at:</p>

<http://www.qt.io/about-us/>

{{< /sponsor >}}

{{< sponsor homepage="https://www.opensuse.org" img="/media/2015/opensuse.png" >}}

<p id="opensuse"><strong>The openSUSE project</strong>
is a worldwide effort that promotes the use of Linux everywhere. openSUSE
creates one of the world's best Linux distributions, working together in an
open, transparent and friendly manner as part of the worldwide Free and Open
Source Software community. The project is controlled by its community and
relies on the contributions of individuals, working as testers, writers,
translators, usability experts, artists and ambassadors or developers. The
project embraces a wide variety of technology, people with different levels
of expertise, speaking different languages and having different cultural
backgrounds. Learn more at:</p>

<https://www.opensuse.org>

{{< /sponsor >}}

{{< sponsor homepage="https://blue-systems.com" img="/media/2015/bluesystems2015.png" >}}

<p id="bluesystems"><strong>Blue Systems</strong>
is a company investing in Free/Libre Computer Technologies. It sponsors several
KDE projects and distributions like Kubuntu and Netrunner. Their goal is to
offer solutions for people valuing freedom and choice.</p>

{{< /sponsor >}}

### Bronze


{{< sponsor homepage="https://www.kdab.com" img="/media/2015/kdab_wee.png" >}}

<p id="kdab"><strong>KDAB</strong>, the world's largest
independent source of Qt knowledge, provides services, training and products
for Qt development. Our engineers deliver peerless software, providing expert
help on any implementation detail or problem. Market leaders in Qt Training,
our trainers are all active developers, ensuring that the knowledge delivered
reflects real-world usage</p>

{{< /sponsor >}}

{{< sponsor homepage="https://www.intel.com" img="/media/2015/intel.png" >}}

<p id="intel"><strong>Intel</strong> creates the
breakthrough technologies that make amazing experiences possible.  As
the world’s leading silicon provider and top
contributor to the Linux kernel, Intel continually advances how people
work and live. For over two decades, Intel's contributions to open-source
projects-from deeply embedded products to massively complex datacenters
have helped ensure that a breadth of solutions run exceptionally well on
Intel® architecture. The combination of consistent innovation and dedicated
execution has helped unlock business opportunities, connect people, and
enhance lives. Open source is bringing amazing experiences to life and Intel
is proud to help make those experiences possible.</p>

{{< /sponsor >}}

{{< sponsor homepage="https://www.igalia.com" img="/media/2015/igalia.png" >}}

<p id="igalia"><strong>Igalia</strong> is an open source
consultancy specialized in the development of innovative projects and
solutions. Likewise, Igalia innovates in
multiple specific areas to deliver the best Open Source technologies and
solutions to its customers. We focus on client-side web technologies including
WebKit and Chromium/Blink, JavaScript engines, optimization of graphical
pipelines, Multimedia, the Linux kernel, Accessibility and more.</p>

{{< /sponsor >}}

### Supporters


{{< sponsor homepage="https://www.redhat.com" img="/media/2014/redhat.png" >}}

<p id="redhat"><strong>Red Hat</strong> is the world's
leading provider of open source solutions, using a community-powered approach
to provide reliable and high-performing cloud, virtualization, storage, Linux,
and middleware technologies.</p>

{{< /sponsor >}}

{{< sponsor homepage="https://openinventionnetwork.com" img="/media/2015/oin.png" >}}

<p id="oin"><strong>Open Invention Network (OIN)</strong> is
the largest patent non-aggression community in history and supports freedom of
action in Linux as a<br />
key element of open source software. Funded by Google, IBM, NEC, Philips,
Red Hat, Sony and SUSE, OIN has more than 1,500 OIN community members and owns
more than 900 global patents and applications. Member cross-licenses and OIN
patent licenses are available royalty free to any party that joins the OIN 
community.</p>

{{< /sponsor >}}

{{< sponsor homepage="https://www.froglogic.com" img="/media/2014/froglogic.png" >}}

<p id="froglogic"><strong>froglogic</strong> is a
software company based in Hamburg, Germany. Their flagship product is
Squish, the market-leading automated testing tool for GUI applications
based on Qt, Java Swing, SWT and JavaFX, Mac OS X Carbon/Cocoa, MFC,
.NET and for HTML/Ajax-based applications running on various operating
systems and in different web browsers.</p>

{{< /sponsor >}}

### Media Partner


{{< sponsor homepage="https://golem.de" img="/media/2015/golem_0.png" >}}

<p id="golem"><strong>golem.de</strong> is an up to date
online-publication intended for professional computer users. It provides
technology insights of the IT and
telecommunications industry. Golem.de offers profound and up to date information
on significant and trending topics. Online- and IT-Professionals, marketing
managers, purchasers, and readers inspired by technology receive substantial
information on product, market and branding potentials through tests,
interviews und market analysis.</p>

{{< /sponsor >}}

### Local Partners


{{< sponsor homepage="https://www.xunta.gal" img="/media/2015/xunta_1.png" >}}

<p id="xunta"><strong>Xunta de Galicia</strong></p>

{{< /sponsor >}}

### Patrons of KDE

[KDE's Patrons](http://ev.kde.org/supporting-members.php)
also support the KDE Community through out the year.
