---
title: Venue
menu:
  "2015":
    weight: 35
    parent: travel
---

![Image of front of computer science building](/media/2015/fic.png)


Akademy 2015 takes place at the [Faculty of Computer Science. Universidad de A
Coruña, A Coruña, Spain](http://www.openstreetmap.org/#map=16/43.3300/-8.4112)

## Address

[Facultade de Informática](http://www.fic.udc.es/).

Campus de Elviña S/N, 15071 A Coruña

## The place

The building provides a great auditorium for full sessions and different sizes
classrooms that can be used for speeches and BOFs.

![Image of auditorium](/media/2015/auditorio.jpg)

![Image of classroom](/media/2015/aula.jpg)

The Computer Science Faculty has a spacious hall with electric tables that also
can be used as hacking zone. In that hall, free fresh water and two microwave
owens are available for the people to use. There are also vending machines.

![Birds eye view image of hall with computer power
outlets](/media/2015/hall.jpg)

![Room level image of hall with computer power outlets](/media/2015/hall2.jpg)
