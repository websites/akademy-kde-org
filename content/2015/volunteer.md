---
title: Call For Volunteers
menu:
  "2015":
    weight: 36
    parent: program
---

Are you attending Akademy 2015 in A Coruña? Help make it unforgettable, and get
an exclusive Akademy 2015 t-shirt in the bargain. Please consider [being a
volunteer](http://community.kde.org/Akademy/2015/Volunteers).

Help is needed for various tasks. Training is provided where necessary or
requested. Some of the tasks where you can help are:

- Registration, pre-registration, information desk and merchandise sales
- Prepare badges prior to the event
- Help with video recording of sessions
- Be a Session Chair; introduce speakers and manage talk rooms
- Set up conference rooms and other infrastructure
- Runners
- Press room staff
- Other jobs that make Akademy run smoothly

Please add your name to the [Volunteer Wiki
page](http://community.kde.org/Akademy/2015/Volunteers).

If you are arriving early, there are opportunities to help during some of the
days before Akademy.

Everybody attending the conference is welcome to be an Akademy 2015 Volunteer.
It doesn't matter where you are from, your age, your skills, whether you have
ever attended a Free Software conference, or have ever been to A Coruña before.
As long as you can communicate in English, you can contribute as a volunteer.
For most people, being a volunteer is one of the high points of Akademy. This is
an opportunity for you to make a difference for the KDE Community.

You should expect that your help will be needed for at least a couple of hours
on a few days. The Team will work with you to make volunteer tasks fit with the
rest of your Akademy experience.

If you want to volunteer, please add your name to the [Wiki
page](http://community.kde.org/Akademy/2015/Volunteers) before 14th
July. This date is important for planning assignments before Akademy begins.
People will be able to sign up after that date as there is usually plenty to do.

The KDE Community, GPUL and the Akademy Team say, “Thank you to all volunteers
in advance!”

If you have any questions, please contact [the Akademy
team](mailto:akademy-team@kde.org) or join us in
#akademy on freenode IRC.

