---
title: Location and dates of Akademy 2015 announced
---

Akademy 2015 is announced to be taking place in A Coruña, Galicia, Spain from
the 25th to 31st July

For more details see the announcement on the dot: [A Coruña, Spain hosting
Akademy
2015](https://dot.kde.org/2015/01/20/akademy-2015-coru%C3%B1a-spain-25-31-july)

