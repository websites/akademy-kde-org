---
title: Swag Bag
menu:
  "2020":
    weight: 29
    parent: details
---


KDE and Akademy sponsors have made available some branded virtual goodies. You
can download them on this page.

Here are some wallpapers:


![Desktop background with Akademy 2020 logo](/media/2020/akademy2020_bg.png)

![Desktop background with Mbition logo](/media/2020/mbition_bg1.png)

![Desktop background with Mbition logo and a car](/media/2020/mbition_bg2.png)
