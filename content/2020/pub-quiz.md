---
title: Pub Quiz
menu:
  "2020":
    weight: 25
    parent: details
---

Join Quiz Masters Dr. Ade and Dr. Paul on a fun filled evening of pub quiz
mania!  
(Please note: Not that kind of doc in the house)

![Cartoon with two men in suspenders](/media/2020/vintage_barmen_image_cred_vecteezy_0.png)

When: Thursday, September 10th at 19:00 UTC
Where: At Akademy
Why: Because it is fun!
How to join: https://meet.kde.org/b/all-wne-ss0

What to Expect:
- 5 Rounds of 6 to 8 Questions,
- Silly Randomness,
- Lots of Laughs,
- The company of new acquaintances and familiar faces.

Round Themes:
- General Knowledge: Questions About Stuff
- What Is It: Know Your Applications
- Places: So You Think You Know Previous Akademy Locations
- Widgets and Thing-a-ma-bobs: Random crap around Ade's house
- ”People”: Questions about the KDE community and beyond! 
