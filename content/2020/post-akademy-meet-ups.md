---
title: Post Akademy Meet-Ups
menu:
  "2020":
    weight: 24
    parent: details
---

Upcoming Event: 12 February 2021 at 17:00 UTC - Fireside Chat with  Luis Falcon
about MyGNUHealth


<button name="button" class="button"
onclick="https://community.kde.org/Fireside_chats">Register!</button>

*If you are new to KDE & Akademy, you will first need a [KDE identity
Account](https://identity.kde.org/)*

![Cartoon with four people dancing in celebration and the word Akademy above
them](/media/2020/post.meet_.jpg)
