---
title: Sponsors
menu:
  "2020":
    weight: 5
---

## Sponsorship Opportunities

A big thank you to our sponsors who help to make this event happen! There are
still sponsorship opportunities.
If you are interested, please see [Sponsoring Akademy](/2020/sponsoring) for more
information, including valuable sponsor benefits.

## Sponsors for Akademy 2020

### Platinum

{{< sponsor homepage="https://www.canonical.com"
img="/media/2020/ubuntu_transparent.png"
>}}

**Canonical** is the company behind Ubuntu, the leading OS for cloud operations.
Most public cloud workloads use Ubuntu, as do most new smart gateways, switches,
self-driving cars and advanced robots. Canonical provides enterprise support and
services for commercial users of Ubuntu. Established in 2004, Canonical is a
privately held company.

<https://www.canonical.com>

{{< /sponsor >}}

{{< sponsor homepage="https://www.kdab.com/" img="/media/2020/kdab3.png" >}}

**KDAB** is the world's leading software consultancy for architecture,
development and design of Qt, C++ and OpenGL applications across desktop,
embedded and mobile platforms. The biggest independent contributor to Qt, KDAB
experts build run-times, mix native and web technologies, solve hardware stack
performance issues and porting problems for hundreds of customers, many among
the Fortune 500. KDAB’s tools and extensive experience in creating, debugging,
profiling and porting complex, great looking applications help developers
worldwide to deliver successful projects. KDAB’s global experts, all full-time
developers, provide market leading training with hands-on exercises for Qt,
OpenGL and modern C++ in multiple languages. 

<https://www.kdab.com>

{{< /sponsor >}}

{{< sponsor homepage="https://mbition.io/" img="/media/2020/mbition_1.png" >}}

**MBition** develops and integrates the next generation of Mercedes-Benz in-car
Infotainment Systems (MBUX) and Advanced Driver Assistance Systems. Our reusable
platform MBiENT enables developers worldwide to develop applications independent
from the car production lifecycle. MBition's embedded operating system work is
augmented by our engagement in Cloud Services and Mobile Apps. We love Open
Source and continue to expand our involvement. Find out more: 

<https://mbition.io/>

{{< /sponsor >}}

### Gold

{{< sponsor homepage="https://www.opensuse.org" img="/media/2015/opensuse.png"
>}}

**The openSUSE project** is a worldwide effort that promotes the use of Linux
everywhere. openSUSE creates one of the world's best Linux distributions,
working together in an open, transparent and friendly manner as part of the
worldwide Free and Open Source Software community. The project is controlled by
its community and relies on the contributions of individuals, working as
testers, writers, translators, usability experts, artists and ambassadors or
developers. The project embraces a wide variety of technology, people with
different levels of expertise, speaking different languages and having different
cultural backgrounds.

<https://www.opensuse.org>

{{< /sponsor >}}

{{< sponsor homepage="https://about.gitlab.com" img="/media/2020/gitlab.png"
>}}

**GitLab**  is a complete DevOps platform, delivered as a single application,
fundamentally changing the way Development, Security, and Ops teams collaborate.
GitLab helps teams accelerate software delivery from weeks to minutes, reduce
development costs, and reduce the risk of application vulnerabilities while
increasing developer productivity. Built on Open Source, GitLab works alongside
its community of thousands of contributors and millions of users to continuously
deliver new DevOps innovations. More than 100,000 organizations from startups to
global enterprise organizations to leading open source projects trust GitLab to
deliver great software at new speeds. 

<https://about.gitlab.com>

{{< /sponsor >}}

### Silver

{{< sponsor homepage="https://pine64.org" img="/media/2020/pine.png"
>}}

**Pine64** is a community driven project that creates Arm and RISC-V devices for
open source enthusiasts and industry partners. Perhaps best known for the
PinePhone, our Linux-only smartphone, and the Pinebook range of laptops, we
strive to deliver devices that you want to use and develop for. Rather than
applying business to a FOSS setting, we allow FOSS principles to guide our
business.

<https://pine64.org>

{{< /sponsor >}}

{{< sponsor homepage="https://dorotac.eu" img="/media/2020/dorotac.png"
>}}

Hi! My name is **Dorota**, and I'm a systems programmer. I sponsor Akademy
because I've been using KDE for over 10 years. Thank you!  I'm starting a new
tech cooperative. Do you need an expert in Linux, GUI programming, or Wayland?
Hire me:

<https://dorotac.eu>

{{< /sponsor >}}

{{< sponsor homepage="https://www.felgo.com" img="/media/2020/felgo.png"
>}}

**Felgo** allows you to accelerate Qt development and improve efficiency with Qt
tools and 200+ APIs. Felgo is an official Qt Technology Partner and Qt Service
Partner and provides:

- Hot Code Reloading of Qt Applications for real-time testing on multiple
  screens and devices
- 200+ APIs for faster Qt development
- Continuous Integration & Deployment (CI/CD) for your Qt project to run
  automated builds and improve your application's quality and stability
- Many open-source examples and demos for UI/UX best practices on Mobile,
  Desktop and Embedded
- Qt consulting services and Qt trainings by Qt experts
- Save 20-80 hours per developer/month and up to 92% code with Felgo APIs and
  development tools - you can download it for free 

<https://www.tuxedocomputers.com>

{{< /sponsor >}}

{{< sponsor homepage="https://www.qt.io" img="/media/2020/tqtc_0.png" >}}

**The Qt Company** (NASDAQ OMX Helsinki: QTCOM) is responsible for Qt
development, productization and licensing under commercial and open-source
licenses. Qt is a C++ based framework of libraries and tools that enables the
development of powerful, interactive and cross-platform applications and
devices.

Used by over a million developers worldwide, Qt is a C++ based framework of
libraries and tools that enables the development of powerful, interactive and
cross-platform applications and devices. Qt’s support for multiple desktop,
embedded and mobile operating systems allows developers to save significant time
related to application and device development by simply reusing one code. Qt and
KDE have a long history together, something The Qt Company values and
appreciates. Code less. Create more. Deploy everywhere.

<https://www.qt.io>

The Future is Written with Qt

{{< /sponsor >}}

{{< sponsor homepage="https://collabora.com" img="/media/2020/collabora.png"
>}}

**Collabora** is a leading global consultancy specializing in delivering the
benefits of Open Source software to the commercial world. For over 15 years,
we've helped clients navigate the ever-evolving world of Open Source, enabling
them to develop the best solutions – whether writing a line of code or shaping a
longer-term strategic software development plan. By harnessing the potential of
community-driven Open Source projects, and re-using existing components, we help
our clients reduce time to market and focus on creating product differentiation.
To learn more, please visit

<https://collabora.com>

Follow us on Twitter

<https://twitter.com/collabora>

{{< /sponsor >}}

{{< sponsor homepage="https://www.froglogic.com" img="/media/2020/froglogic2.png"
>}}

**froglogic GmbH** is a global leader in the software test automation market,
providing state-of-the-art solutions to enhance software quality in any industry
context.  froglogic offers cutting-edge tooling to support GUI test automation,
code coverage analysis and test result management, enabling customers to assess
and steer their Quality Assurance efforts across an application’s lifecycle.
With products transforming the DevOps process and enabling users to develop and
ship high-quality code, froglogic was recognized in the 2018 and 2019 Gartner
Magic Quadrant for Software Test Automation. froglogic supports over 3,500
customers in diverse industry segments worldwide, with headquarters in Hamburg,
Germany and additional offices in the USA and Poland. To lear more, visit:

<https://www.froglogic.com>

{{< /sponsor >}}


### Bronze

{{< sponsor homepage="https://www.codethink.co.uk" img="/media/2020/codethink.png"
>}}

**Codethink** specialises in system-level Open Source software infrastructure to
support advanced technical applications, working across a range of industries
including finance, automotive, medical, telecoms. Typically we get involved in
software architecture, design, development, integration, debugging and
improvement on the deep scary plumbing code that makes most folks run away
screaming. 

<https://www.codethink.co.uk>

{{< /sponsor >}}

## Supported by

{{< sponsor homepage="https://www.tuxedocomputers.com"
img="/media/2020/tuxedo_computers.png"
>}}

**TUXEDO Computers** builds tailor-made hardware and all this with Linux.
TUXEDO Computers are individually built computers/PCs and notebooks which are
fully Linux compatible, i. e. Linux hardware in tailor-made suit.

A/V and Streaming research has been kindly supported by Tuxedo Computers.
<https://www.tuxedocomputers.com>

{{< /sponsor >}}
