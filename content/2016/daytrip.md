---
title: Daytrip
menu:
  "2016":
    weight: 2
hideSponsors: true
---

As the weather forecast is looking good for Wednesday we shall be going to
[Pfaueninsel](https://en.wikipedia.org/wiki/Pfaueninsel).

*The day-trip operates on a pay-your-own-way system, basically buy your own
tickets for things.*

You will need a normal Berlin AB travel ticket for this journey. Take S7 to
Wansee S-Bahn station (nearest station to TU is Zoologischer Garten), from the
platform go down the steps, turn right and go to the end of the tunnel and up
the stairs. Continue to the bus stop just after the shelter, we are getting bus
218 to Pfaueninsel. The buses are only once an hour at 13:51,14:51 etc lasting
10 minutes. It is the last stop on the bus that we get off, this will be where
the majority of other people get off as well. It is then a short ferry journey
over to the Island (4€ return, pay as you get on the ferry. there is no further
charge for the island).

There will be a group leaving from TU at 12:50 to get the U2 from
Ernst-Reuter-Platz to Zoologischer Garten, then the S7 leaving at 13:22.

We will spend the afternoon there. Where you can wander about exploring the
island and chatting to other people in KDE. Near the middle of the island there
is a place to buy drinks and snacks. The palace tour is only available in German
and for most people probably not that interesting. The island "closes" at 19:00.

On the mainland at the ferry crossing is [Wirtshaus zur
Pfaueninsel](http://www.pfaueninsel.de/) however the
last bus back to Wansee is at 20:04 so it is recommended to go back into the
centre of Wansee to get food and drinks where there is [Loretta am
Wannsee](http://www.loretta-berlin.de/) next
to the station. There are also some shops at the station where you could buy
food and drink to take down to the seating at the edge of the river at
Ronnebypromenade.

Some people may wish to stop off at Grunewald S-Bahn station to visit the [Gleis
17](http://www.memorialmuseums.org/eng/staettens/view/338/Mahnmal-Gleis-17-%E2%80%93--Berlin-Grunewald) memorial, you can then just get on the next train to continue to Wansee. If
you are interested in doing this there will be a group leaving slightly earlier
from TU at 12:20.

