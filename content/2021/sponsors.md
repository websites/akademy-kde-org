---
title: Sponsors
menu:
  "2021":
    weight: 5
---

## Sponsorship Opportunities

A big thank you to our sponsors who help to make this event happen! There are
still sponsorship opportunities available.  If you are interested, please see
[Sponsoring Akademy](/2021/sponsoring) for more information, including valuable
sponsor benefits.

## Sponsors for Akademy 2021

### Platinum

{{< sponsor homepage="https://mbition.io/" img="/media/2021/mbition_1.png" >}}

At **MBition**, we strive to bring digital luxury
to mobility users around the world. To accomplish this, we are redefining how
software is developed within the automotive field. As a 100% subsidiary of
Mercedes-Benz AG. We develop and integrate the next generation of Mercedes-Benz
in-car Infotainment systems (MBUX) and Advanced Driver Assistance Systems. Our
reusable platform MBiENT enables developers worldwide to develop applications
independent from the car production lifecycle. MBition's embedded operating
system work is augmented by our engagement in Cloud Services and Mobile Apps.
We, at MBition are proud to be a part of the KDE Akademy as across our
developments we employ and contribute to the OSS community allowing our
knowledge to help others, and their knowledge to help us. OSS will play an
ever-larger role at MBition, and we will further invest into this area over the
next years. Find out more:

<https://mbition.io/>

{{< /sponsor >}}

{{< sponsor homepage="https://www.kdab.com/" img="/media/2021/kdab3.png" >}}

**KDAB** is the world's leading software consultancy
for architecture, development and design of Qt, C++ and OpenGL applications
across desktop, embedded and mobile platforms. The biggest independent
contributor to Qt, KDAB experts build run-times, mix native and web
technologies, solve hardware stack performance issues and porting problems for
hundreds of customers, many among the Fortune 500. KDAB’s tools and extensive
experience in creating, debugging, profiling and porting complex, great looking
applications help developers worldwide to deliver successful projects. KDAB’s
global experts, all full-time developers, provide market leading training with
hands-on exercises for Qt, OpenGL and modern C++ in multiple languages. 

<https://www.kdab.com>

{{< /sponsor >}}

{{< sponsor homepage="https://www.qt.io" img="/media/2021/tqtc_0.png" >}}

**The Qt Company** (NASDAQ OMX Helsinki: QTCOM) is
responsible for Qt development, productization and licensing under commercial
and open-source licenses. Qt is a C++ based framework of libraries and tools
that enables the development of powerful, interactive and cross-platform
applications and devices.

Used by over a million developers worldwide, Qt is a C++ based framework of
libraries and tools that enables the development of powerful, interactive and
cross-platform applications and devices. Qt’s support for multiple desktop,
embedded and mobile operating systems allows developers to save significant time
related to application and device development by simply reusing one code. Qt and
KDE have a long history together, something The Qt Company values and
appreciates. Code less. Create more. Deploy everywhere.

<https://www.qt.io>

The Future is Written with Qt

{{< /sponsor >}}

{{< sponsor homepage="https://about.gitlab.com" img="/media/2021/gitlab.png" >}}

**GitLab** is a complete DevOps platform, delivered
as a single application, fundamentally changing the way Development, Security,
and Ops teams collaborate. GitLab helps teams accelerate software delivery from
weeks to minutes, reduce development costs, and reduce the risk of application
vulnerabilities while increasing developer productivity. GitLab provides
unmatched visibility, radical new levels of efficiency and comprehensive
governance to significantly compress the time between planning a change and
monitoring its effect.


Built on Open Source, GitLab works alongside its community of thousands of
contributors and millions of users to continuously deliver new DevOps
innovations. More than 100,000 organizations from startups, to leading open
source projects, to global enterprise organizations, trust GitLab to deliver
great software at new speeds. 

<https://about.gitlab.com/solutions/open-source>

{{< /sponsor >}}

{{< sponsor homepage="https://getfedora.org" img="/media/2021/fedora2021.png" >}}

**The Fedora Project** s a community of people who
create an innovative, free, and open source platform for hardware, clouds, and
containers that enables software developers and community members to build
tailored solutions for their users.

<https://getfedora.org>


The KDE Special Interest Group (SIG) is a group of Fedora contributors that
maintain popular KDE packages, take care of KDE-related documentation, artwork
and other KDE specific tasks. Learn more at 

<https://fedoraloveskde.org/>

{{< /sponsor >}}


### Gold

{{< sponsor homepage="https://www.opensuse.org" img="/media/2015/opensuse.png"
>}}

**The openSUSE project** is a worldwide effort that
promotes the use of Linux everywhere. openSUSE creates one of the world's best
Linux distributions, working together in an open, transparent and friendly
manner as part of the worldwide Free and Open Source Software community. The
project is controlled by its community and relies on the contributions of
individuals, working as testers, writers, translators, usability experts,
artists and ambassadors or developers. The project embraces a wide variety of
technology, people with different levels of expertise, speaking different
languages and having different cultural backgrounds.

<https://www.opensuse.org>

{{< /sponsor >}}


{{< sponsor homepage="https://www.canonical.com"
img="/media/2021/ubuntu_transparent.png"
>}}

**Canonical** is the publisher of Ubuntu, the OS
for most public cloud workloads as well as the emerging categories of smart
gateways, self-driving cars and advanced robots. Canonical provides enterprise
security, support and services to commercial users of Ubuntu. Established in
2004, Canonical is a privately held company.</p>

<https://www.canonical.com>

{{< /sponsor >}}

{{< sponsor homepage="https://remarkable.com/" img="/media/2021/remarkable.png" >}}

**remarkable**'s vision is “Better thinking
through technology”. Since launching our first piece of ground-breaking E Ink
hardware in 2017, our digital paper tablets have helped users to focus on what
really matters - without any distractions. For our devices, we created our own
Linux distribution and companion apps and use current technology - including
some from KDE - to create amazing user experiences, lead by design that stays
true to our customer-focused goals. We are based in Oslo, Norway and have a
great, fast-growing team innovating in hardware design, amazing software
features, and our finished products. 

<https://remarkable.com>

{{< /sponsor >}}

### Silver

{{< sponsor homepage="https://pine64.org" img="/media/2021/pine2021.png"
>}}

**Pine64** is a community driven project that
creates Arm and RISC-V devices for open source enthusiasts and industry
partners. Perhaps best known for the PinePhone, our Linux-only smartphone, and
the Pinebook range of laptops, we strive to deliver devices that you want to use
and develop for. Rather than applying business to a FOSS setting, we allow FOSS
principles to guide our business.

<https://pine64.org>

{{< /sponsor >}}

{{< sponsor homepage="https://shells.com" img="/media/2021/shells.png"
>}}

At **Shells** we are closing the digital divide by
providing simple, secure, and affordable access to a powerful, future-proof
computing solution that is accessible on any device. In addition to providing a
powerful computing option, we also offer a variety of pre-installed Linux
distributions which give our users complete flexibility and control of their
device while also opening new doors for a potential new Linux user.

<https://shells.com>

{{< /sponsor >}}

{{< sponsor homepage="https://www.tuxedocomputers.com" img="/media/2021/tuxedo.png"
>}}

**TUXEDO Computers** builds tailor-made hardware with Linux! For a wide variety
of computers and notebooks - all of them individually built und prepared - we
are the place to go. From lightweight ultrabooks up to full-fledged AI
development stations TUXEDO covers every aspect of modern Linux-based computing.
In addition to that we provide customers with full service at no extra cost:
Self-programmed driver packages, tech support, fully automated installation
services and everything around our hardware offerings.

<https://www.tuxedocomputers.com>

{{< /sponsor >}}

{{< sponsor homepage="https://collabora.com" img="/media/2021/collabora.png"
>}}

**Collabora** is a leading global consultancy specializing in delivering the
benefits of Open Source software to the commercial world. For over 15 years,
we've helped clients navigate the ever-evolving world of Open Source, enabling
them to develop the best solutions – whether writing a line of code or shaping a
longer-term strategic software development plan. By harnessing the potential of
community-driven Open Source projects, and re-using existing components, we help
our clients reduce time to market and focus on creating product differentiation.
To learn more, please visit

<https://collabora.com>

Follow us on Twitter

<https://twitter.com/collabora>

{{< /sponsor >}}

### Bronze

{{< sponsor homepage="https://kde.slimbook.es" img="/media/2021/slimbook_0.png"
>}}

**SLIMBOOK** has been in the computer manufacturing business since 2015. We
build computers tailored for Linux environments and ship them worldwide. Our
main goal is to deliver quality hardware with our own apps combined with an
unrivaled tech support team to improve the end-user experience. We also firmly
believe that not everything is about the hardware. SLIMBOOK has been involved
with the community from the beginning, taking on small local projects to bring
the GNU/Linux ecosystem to everyone. We have partnered with state-of-the-art
Linux desktops like KDE, among other operating systems. And of course, we have
our very own Linux academy, "Linux Center," where we regularly impart free
Linux/FOSS courses for everyone. If you love Linux and need quality hardware to
match, BE ONE OF US.

<https://kde.slimbook.es>

{{< /sponsor >}}

