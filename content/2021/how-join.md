---
title: How to Join
menu:
  "2021":
    weight: 25
    parent: details
---

![Image with an astronaut on Mars and the words "AKADEMY
TWENTYONE"](/media/2021/registration_banner.jpg)

[Interact & Watch the Talks](https://go.kde.org/matrix/#/#attend-akademy:kde.org)  
Use the button above to enter the Akademy space on KDE's Matrix event platform.

*You will need a Matrix account to see the talks and participate in the discussion. This is different from your KDE Identity account.*  
*Note that you may be asked to enable the Spaces feature on Matrix. Please do so to enjoy the conference.*



[Track Room 1 (Only Video)](https://akademy.kde.org/2021/track-1)


[Track Room 2 (Only Video)](https://akademy.kde.org/2021/track-2)



Watch on YouTube  
[YouTube Playlist](https://www.youtube.com/playlist?list=PLsHpGlwPdtMq6pJ4mqBeYNWOanjdIIPTJ)


## Trainings, Workshops, BoFs, and Social Events



[Training Sessions](https://conf.kde.org/event/1/timetable/#20210618)


[BoFs & Workshops](https://community.kde.org/Akademy/2021/AllBoF)  
*Birds of a Feather (BoFs) are small topic-based meetings and discussions.*


[Social Events](https://akademy.kde.org/2021/social-events)


## Sponsor Office Hours

[Collabora](https://meet.kde.org/b/akademy-officehours-collabora)  
*Saturday, 19 June at 12:30 pm UTC*

[Fedora](https://meet.kde.org/b/akademy-officehours-fedora)  
*Thursday, 24 June at 12:00 pm UTC*

[MBition](https://meet.kde.org/b/akademy-officehours-mbition)  
*Thursday, 24 June at 13:00 pm UTC*

[GitLab](https://meet.kde.org/b/akademy-officehours-gitlab)  
*Thursday, 24 June at 14:00 pm UTC*
