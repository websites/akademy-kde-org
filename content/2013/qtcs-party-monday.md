---
title: QtCS Party on Monday
menu:
  "2013":
    weight: 50
    parent: program
---

As an Akademy attendee, you are also invited to join the Qt Contributors Summit
party on Monday evening at the Bilbao Arena. The party will take place at the
private club inside of the Bilbao Arena. A concert by the Iosebeatles will
entertain you, while you enjoy nice pintxos and free drinks. The party starts at
20.00

Address: Palacio de deportes "Bilbao Arena", Avenida Askatasuna 13.

There will be signs to lead you to the correct entrance to the party since the
arena is a huge building.

Public transport from ETSI (the conference venue):

- Take the tram till the last stop, located at Atxuri, and walk 10 for 6-10
  minutes uphill until you reach the building.
- Take the metro to stop "Casco Viejo" and ask how to go to "Bilbao Arena". 15
  minutes walking from there.

![Image of party location in Bilbao Arena](/media/2013/encabezado2_0.jpg)
