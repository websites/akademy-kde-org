---
title: Daytrip on Wednesday
menu:
  "2013":
    weight: 51
    parent: program
---

<div style="float: right; padding: 1ex; margin: 1ex; width:
320px; "> 
<img src="/media/2013/Causeway_to_the_Ermita_de_San_Juan%2C_Biskaia%2C_Spain_02-2005.jpg" width="320"/>
<br />
<figcaption><a href="http://en.wikipedia.org/wiki/File:Causeway_to_the_Ermita_de_San_Juan,_Biskaia,_Spain_02-2005.jpg">
Phillip Capper CC-BY-SA</a></figcaption>
</div>


Thanks to our fantastic local team, this year there will be a day trip during
Akademy week.

It will happen on Wednesday, 17 July in the afternoon and evening. It will be a
bus trip to the beautiful island of
[Gaztelugatxe](http://en.wikipedia.org/wiki/Gaztelugatxe) and other villages on the
Basque coast. We will also visit the beach so bring your swimming gear! We will
leave from the venue after lunch, at approx. 12.30.

The trip will include a dinner at a typical cider brewery, a sidrería. Don't
miss this!

The buses and dinner are sponsored, so you don't need to worry about extra
costs.

In order for us to plan how many buses we'll need, please [register for the
day](https://conf.kde.org/Akademy2013)
trip. Once you're signed in, go to the registration page and you will find an
extra check box saying "Attend day-trip on Wednesday 12:30 - 23:00". Please
register by Thursday, 11 July!

Please note: **There won't be any BoF's on Wednesday afternoon**. However, you can
still schedule a BoF between 09:30 and 12:00.
