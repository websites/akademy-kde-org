---
title: Travel to Brno
menu:
  "2014":
    weight: 52
    parent: travel
---

When planning your travel to Akademy, please remember that the **KDE e.V. Annual
General Meeting is on Friday, 5th September**.

## By Plane

[Vienna International Airport](http://www.viennaairport.com/) (VIE, in the
neighbouring country of Austria, 22 million passengers a year)

- **the fastest way to get to Brno if you fly!**
- the biggest airline hub in Central Europe, flights to many destinations all
  over the world.
- KLM fly here from Amsterdam
- the easiest way to get from the airport to Brno is by [Student Agency
  bus](http://jizdenky.studentagency.cz/?wicket:interface=:0:1:::)
  (online booking possible). The buses go directly from the Vienna airport to
  Brno and take 2 hours. Tickets are cheaper than train tickets. Another option is
  to take a train or bus to the city center and take an [EuroCity (EC)
  train](http://en.wikipedia.org/wiki/EuroCity) to
  Brno from there. The EC train stops at several stations in Vienna. You can check
  the times on the website of the [Austrian train
  services](http://fahrplan.oebb.at/bin/query.exe/en) 

[Václav Havel Airport Prague](http://www.prg.aero/en/) (PRG, 11 million)

- the second busiest airport in the region.
- there are no direct coaches between the airport and Brno, but it's possible to
  book the journey as one ticket with [Student
  Agency](http://jizdenky.studentagency.cz/?wicket:interface=:0:1:::) with a 
  connection at the Prague main bus station. Czech Railways provides shuttle buses
  between the airport and the Prague main train station.
- to get from the Prague airport to Brno takes at least 3.5 hours by public
  transport, which is why we recommend you fly to Vienna.

[Bratislava Airport](http://www.airportbratislava.sk/en/passengers/) (BTS, in
the neighbouring country of Austria, 1.4 million)

- good option for cheap flights within Europe because Bratislava Airport is a
  hub of Ryanair. Otherwise not very recommended for traveling to Brno.
- there is no direct transport connection between the airport and Brno. You have
  to take a city bus to the center and take a coach or train to Brno from there.
  It takes at least three hours.

[Brno Airport](http://www.airport-brno.cz/index.php?id=0&lang=en) (BRQ, 0.6
million)

- a small international airport near Brno. Just a few regular low-cost flights.
  A good option if you're coming from London (Stansted - Ryanair, Luton -
  Wizzair), Eindhoven (Wizzair), or Moscow.
- City bus #76 goes from the airport to the city center (30 minutes). You can
  also take a taxi: approx. 300 CZK/€11. (1 Czech Republic Koruna is about
  .037€; 1€=~27.36 CZK).

There are also two small international airports in
[Ostrava](http://www.airport-ostrava.cz/) (flights to Paris and London) and
[Pardubice](http://www.airport-pardubice.cz/) (flights to Saint Petersburg and
Moscow). Both have good train and bus connections to Brno.

![Map showing airports near Brno](/media/2014/LKTB_04.png)
<figcaption><center>
Airports near Brno<br> 
<small><a
href="https://commons.wikimedia.org/wiki/File:LKTB_04.png">Public
domain
</a></small>
</center></figcaption>

## By Train

Brno has good train connections to several European cities. Train is the fastest
and most convenient means of transportation between big cities in the region.
All intercity trains arrive and depart at the main station [Brno hl.
n.](http://en.wikipedia.org/wiki/Brno_hlavn%C3%AD_n%C3%A1dra%C5%BE%C3%AD), which
is a hub of public transport in the city.

### Train timetables:

- Prague - trains between Prague and Brno go hourly, 2.5 hours
- Vienna - trains between Vienna Meidling and Brno go every hour, 2 hours
- Bratislava - from Bratislava Hlavna Stanica hourly, 1.5 hour
- Budapest - from Budapest-Keleti pu several trains every day, 5 hours
- Warsaw - from Warszawa Centralna several times every day (usually one
  connection), 7 hours
- Berlin - from Berlin Hauptbahnhof several trains every day, 7.5 hours

When travelling from Prague with Czech railways in group, you can get group
rebate, which is calculated as follows:

- 1st traveler pays the whole price
- 2nd traveler pays 75% of what the first one paid
- 3rd and any other pays 50% of what the first paid

So for example if Prague - Brno costs 210 CZK for one, it will be 368 CZK for
two, 473 CZK for three, etc.

To get timetables for trains as well as local public transport in Prague and
Brno (and other cities in Czech republic), visit [Idos.cz](http://www.idos.cz/).

## By Bus

Brno is part of the European bus network; all connections and prices are similar
to trains. [Student Agency](http://www.studentagency.eu/) and
[Eurolines](http://www.eurolines.com/) provide buses to many European cities.
Most buses of Eurolines arrive at Brno-Zvonarka. Student Agency buses arrive at
Grand Hotel which is on the same street as the main train station.  ## By Car

Brno is well-connected to other cities by highway. You can easily get to
neighboring countries by car. To use Czech highways you have to pay a toll
(price 350 CZK/13 € for a 10-day sticker, you can get them at any gas station).
Travel time examples:

- Prague - 210 km, 2 hours
- Bratislava - 130 km, 1.3 hour
- Vienna - 143 km, 1.8 hour
- Budapest - 326 km, 3 hours
- Munich - 587 km, 5.3 hours
- Berlin - 555 km, 5.2 hours
