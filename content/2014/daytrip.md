---
title: Daytrip
menu:
  "2014":
    weight: 46
    parent: program
---

It has become somewhat of a tradition to have a daytrip sometime during
[Akademy](/2014).  This year, the daytrip will be on Wednesday, September 10th
in the afternoon and evening. Akademy attendees who want a change of scene are
invited to this ***Adventure at Brno Reservoir***. There won't be any BoFs on
Wednesday afternoon.

![Image of outdoor restaurant](/media/2014/RelaxErlaxazioa700JRBYSA.jpg)
<figcaption><center> Just relax!<br> <small>by Martin Klapetek (CC BY)</small>
</center></figcaption>

## Brno Reservoir

Brno Reservoir is a popular recreational area with both locals and visitors.
Both sides of the lake are forested; there are opportunities for swimming (for
brave souls), water sports, hiking and cycling. The [Veveří
Castle](http://en.wikipedia.org/wiki/Veve%C5%99%C3%AD_Castle) is a popular
destination. There is a bridge for cyclists and hikers over the Reservoir close
to the Castle
([map](https://www.google.com/maps/search/castle/@49.2576068,16.4623715,491m/data=!3m1!1e3!4m5!2m4!3m3!1scastle!2sBrno+Reservoir,+635+00+Brno,+Czech+Republic!3s0x471290ae3e579d2d:0xb2131f9b1b9690b0)).
In addition there are numerous bars and cafes around the Reservoir. Take a trip
back in time with a ferry boat ride to the quaint town of Veverská Bítýška. The
ferry ([timetable and map
PDF](http://dpmb.cz/default.aspx?seo=download&id=2231)) also goes to the Castle
(depart at the "Hrad Veveří" stop). People should plan their own picnic-type
games and activities...football, catch, Frisbee, hiking.

In the early evening, the group will gather at the bottom of the Reservoir for
picnics and barbecue. There is a [supermarket next to the tram stop at
Přístaviště](http://www.penny.cz/Homepage/Homepage_Ne_18_8_/pe_Home.aspx) (the
Píškova location) for people to buy supplies. Akademy will provide BBQ cookers,
charcoal and cooking implements.

Join the adventure at Brno Reservoir, a perfect outing for grassroots friends.

![Image of Brno Reservoir](/media/2014/BrnoReservoir700.JPG)
<figcaption><center>Brno Reservoir</center></figcaption>

## Getting there

[Where's the
Rez](https://www.google.com/maps/dir/Czech+Technology+Park,+Technick%C3%A1+15,+616+00+Brno,+Czech+Republic/P%C3%AD%C5%A1kova,+Brno,+Czech+Republic/@49.2198628,16.5189249,13z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x47129403cbc87cf5:0x75a32fc89df2fddd!2m2!1d16.577048!2d49.226239!1m5!1m1!1s0x471296d99d7e1109:0xece041fe20c6afa0!2m2!1d16.5232671!2d49.2269804!3e0)?
Take bus 53 to "Technologický Park" and change to tram 12. At "Konečného
náměstí", change to tram x3 in direction "Rakovecká". Get out at station
"Přístaviště". From there, it's a [5 minute walk to the
Reservoir](https://www.google.com/maps/dir/49.2274464,16.5232996/49.230312,16.5165727/@49.2295263,16.5179684,16z/data=!4m4!4m3!1m0!1m0!3e2).

For a different adventure, take bus 53 to "Skácelova" (direction "Semilasso"),
change to bus 30 (direction "Bystrc"). At "Podlesí", change to tram x3 and
depart at "Přístaviště".

Daytrippers should make their own transportation arrangements and purchase
tickets. There is no set itinerary; transportation will not be provided by
Akademy.

## No BoFs on Wednesday afternoon

There won't be any BoFs on Wednesday afternoon, but they can be scheduled
Wednesday morning until 12:30.

