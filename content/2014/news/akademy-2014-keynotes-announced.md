---
title:  Akademy 2014 Keynotes Announced
---

Akademy 2014 will kick off on September 6 in Brno, Czech Republic; our keynote
speakers will be opening the first two days. Continuing a tradition, the first
keynote speaker is from outside the KDE community, while the second is somebody
you all know. On Saturday, Sascha Meinrath will speak about the dangerous waters
he sees our society sailing into, and what is being done to help us steer clear
of the cliffs. Outgoing KDE e.V. Board President, Cornelius Schumacher, will
open Sunday's sessions with a talk about what it is to be KDE and why it
matters.

For the full details see the article on the
[dot](https://dot.kde.org/2014/07/30/akademy-2014-keynotes-sascha-meinrath-and-cornelius-schumacher)

