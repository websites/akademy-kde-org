---
title: Accommodation
menu:
  "2014":
    weight: 51
    parent: travel
---

**If you are applying for [accommodation support](https://akademy.kde.org/2014/register)
please do not book your own accommodation, unless you hear back that your application was
unsuccessful and still plan to attend**

## Hostels
### Palacký Hostel **

**This is the recommended accommodation (for more comfort Palacky Hotel is
located in the same building)**

The hostel is located on campus 5 minute walk from the venue.  Accommodation is
provided in two-bed rooms containing shower and toilet, a fridge and wired
internet connection (you can borrow a cable from reception).  Each floor has
kitchens with 2x2 hotplates each. There are TV and study rooms as well as a
meeting room available for late night hacking. There is a small shop and cafe
(open every day) along with a cafeteria providing lunches, pizza and baguettes
(open Monday - Friday).  
**Price**: 275 Kč (approx 10€) / bed / night + 25 Kč /
day for internet connection. There is a fee of 117 Kč / night on top of the
normal price for the free bed if there is only one person staying in the room
for any of the booking period.  
<b style="color:red;">price note</b>: We have a
10% discount. If you think it is not applied (compare to the prices listed
above), please notify the lady that you are attending Akademy and should have a
lower price. If it doesn't help, notify m-at-rtinbriza.cz  
**Capacity**: "there's still a reserve" (as of 28/Aug/14)  
**Web**: http://www.hotel-palacky.cz  
**How to reserve**: Booking is for a room. So to
share you need to find someone before you make the booking. To help with this we
have created a wiki page to use to find a room-mate. One of you should then
email recepce@hotel-palacky.cz using the reference "Akademy" specifying the
dates you and your room-mate want to stay and that it is the hostel rather than
the hotel you should also say if you would like breakfast (115 Kč/5€ extra per
day). Payment on arrival.

## Hotels ### Palacký Hotel ***

Located in the same building as the hostel and with access to the same
facilities.  The rooms are provided with two beds, fridge, wired internet
connection, TV, phone, shower and toilet.  
**Price**: 390 Kč (approx 16€) for
two people OR 350 Kč (approx 14€) for a single person in the room  
**Capacity**: full (as of 28/Aug/14)  
**Web**: http://www.hotel-palacky.cz  
**How to reserve**: email recepce@hotel-palacky.cz using the reference "Akademy"
specifying you want to stay at the hotel and if you would like breakfast (115
Kč/5€ extra per day). Payment on arrival.  

### Pension Edison ***

Located about 5 minutes walk from the venue.  Two-bed rooms only, three
different levels of comfort. Basic includes a TV, separate bathroom and
internet.  One room is disabled-friendly.  
**Price**: Starting at 1750 Kč
(approx 64€) single or 2020 Kč (approx 74€) twin per night (10% discount for 3
nights and more)  
**Capacity**: 14 people  
**Web**: http://www.pension-edison.cz/  
**How to reserve**: booking form

### Pension Venia

Located about 10 minutes from the venue using the public transport or 20 taking
a walk.  
**Price**: Starting at 930 Kč / room / night (approx 34€)
**Capacity**: 15 people  
**Web**: http://www.pensionvenia.cz/  
**How to reserve**: email info@pensionvenia.cz  

### Hotel A-Sport ****

Located about 15 minutes walk from the venue - taking public transport doesn't
make much sense from its location.  There is also a restaurant providing lunches
every working day.  You can order half board for 120 Kč per day.  
**Price**: 1240 Kč (approx 45€) / 1-bed room / night; 1590 Kč (approx 58€) / 2-bed room /
night; 2160 Kč (approx 79€) / 3-bed room / night; 2700 Kč (approx 99€) / 4-bed
room / night; 430 Kč (approx 16€) / extra bed (15% discount for ISIC holders and
seniors over 65) 
**Capacity**: about 70 people  
**Web**: http://www.a-sporthotel.cz/en/  
**How to reserve**: booking form  

### Barceló Brno Palace *****

New luxury hotel in Spanish style with a high-quality restaurant.  Located in
the city centre, few minutes walk to Náměstí svobody (Liberty square) and about
5 kilometers from the venue.  
**Price**: Starting at 137€  
**Web**:
[http://www.barcelo.com/BarceloHotels/en_GB/hotels/Czech-Republic/Brno/ho](http://www.barcelo.com/BarceloHotels/en_GB/hotels/Czech-Republic/Brno/hotel-barcelo-brno-palace/general-description.aspx)...  
**How to reserve**: booking form

