---
title: News
menu:
  "2014":
    weight: 2
---


## [Talk Recordings Available](/2014/news/talk-recordings-available)

Videos of all of the Akademy Talks are now available online to watch in your own
time.

You can access them from the Schedule:
https://akademy.kde.org/2014/conference-schedule From here, if you click on a
talk, you will see that there are links (the videos are here) and files (the
slides will be here if they have been uploaded).

A small note, due to technical issues with equipment at the venue, some of the
audio isn't great - we can only apologise about this.

[Read more](/2014/news/talk-recordings-available)

## [Akademy 2014 Program announced](/2014/news/akademy-2014-program-announced)

The Akademy Program Committee is excited to announce the Akademy 2014 Program.
It is worth the wait! We waded through many high quality proposals and found
that it would take more than a week to include all the ones we like. However we
managed to bring together a concise and (still packed) schedule. (Full details
in the [dot
story](https://dot.kde.org/2014/08/04/akademy-2014-program-schedule-fast-fun-inspiring))

[Read more](/2014/news/akademy-2014-program-announced) 

## [Pre-order Akademy 2014 t-shirt](/2014/news/pre-order-akademy-2014-t-shirt)

Akademy 2014 t-shirts are now available to [pre-order](/2014/tshirt) until the
10th of August

[Read more](/2014/news/pre-order-akademy-2014-t-shirt) 

## [Akademy 2014 Keynotes Announced](/2014/news/akademy-2014-keynores-announced)

Akademy 2014 will kick off on September 6 in Brno, Czech Republic; our keynote
speakers will be opening the first two days. Continuing a tradition, the first
keynote speaker is from outside the KDE community, while the second is somebody
you all know. On Saturday, Sascha Meinrath will speak about the dangerous waters
he sees our society sailing into, and what is being done to help us steer clear
of the cliffs. Outgoing KDE e.V. Board President, Cornelius Schumacher, will
open Sunday's sessions with a talk about what it is to be KDE and why it
matters.

For the full details see the article on the
[dot](https://dot.kde.org/2014/07/30/akademy-2014-keynotes-sascha-meinrath-and-cornelius-schumacher)

[Read more](/2014/news/akademy-2014-keynotes-announced)

## [Are you going to Akademy 2014](/2014/news/are-you-going-akademy-2014)

If you are joining us for Akademy 2014 let others know by using the badges on
the [wiki](https://community.kde.org/Akademy/2014/badges)

[Read more](/2014/news/are-you-going-to-akademy-2014) 

## [Call For Volunteers](/2014/news/call-volunteers)

Are you attending Akademy 2014 in Brno? Help make it unforgettable, and get an
exclusive Akademy 2014 t-shirt in the bargain. Please consider [being a
volunteer](/2014/volunteer).

Help is needed for various tasks. Training is provided where needed or
requested.

[Read more](/2014/news/call-volunteers)

## [Akademy 2014 Call for Papers](/2014/news/akademy-2014-call-papers)

The goal of the conference section of Akademy is to learn and teach new skills
and share our passion around what we're doing in KDE with each other.

For the sharing of ideas, experiences and state of things, we will have short
Fast Track sessions in a singe track section of Akademy. Teaching and sharing
technical details is done through longer sessions in the multi-track section of
Akademy.

If you think you have something important to present, please tell us about it.
If you know of someone else who should present, please nominate them. For more
details see the proposal guidelines and the [Call for Papers](/2014/cfp). The
[Submission](https://conf.kde.org/en/Akademy2014/cfp/session/new) Deadline is
Sunday 18th May, 23:59:59 CEST.

[Read more](/2014/news/akademy-2014-call-for-papers)

## [Akademy 2014 Location & Dates announced](/2014/news/akademy-2014-location-dates-announced)

Today KDE announced the location of Akademy 2014 to be Brno, Czech Republic from
6th - 12th September.

More information is available in the story on the dot: [Akademy 2014 - Brno,
Czech Republic](http://dot.kde.org/2013/12/19/akademy-2014-brno-czech-republic)

[Read more](/2014/news/akademy-2014-location-dates-announced)
