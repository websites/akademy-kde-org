---
title: Akademy 2025
layout: home

community:
  title: Come Join Us
  description: Meet hundreds of KDE contributors and other actors in the open source world.

images:
  - /media/2022/akademy2022-groupphoto-med.jpg

menu:
  "2025":
    name: 2025
    weight: 1
  main:
    name: "2025"
    weight: 975
hideSponsors: true
scssFiles:
- /scss/2025/home.scss
---
