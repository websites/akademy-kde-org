---
title: Pre-order Akademy 2018 T-Shirt
menu:
  "2018":
    weight: 31
    parent: details
---

<div style="float:right; text-align: center; padding: 1ex; margin: 1ex;
width: 300px; border: 1px solid grey;">
<a href="/media/2018/tshirt-mock-attendees_0.png">
  <img src="/media/2018/tshirt-mock-attendees-wee_0.png" 
  alt="Image of front and sleeve of pink Akademy 2018 t-shirt">
</a>
<figcaption>
Mockup of Akademy 2018 t-shirt by Jens Reuterberg
</figcaption>
</div>


Following on from previous years we have the option for buying an official
Akademy 2018 t-shirt. Attendees can pre-order a t-shirt, pick it up at Akademy
and pay a reduced price.

It's hard for the team to guess how many of each size shirt to order. Sometimes
people aren't able to get the right size because they have sold out, and we
often end up with leftovers of other sizes.

This year's design was created by Jens Reuterberg and is inspired by the [Strauss
Monument in Stadtpark,
Vienna](https://commons.wikimedia.org/wiki/File:Strauss_Monument.JPG)

If you are definitely coming to Akademy 2018, you can pre-order the size of
t-shirt that you want. Then pick it up and pay during the conference.
Pre-ordered t-shirts have a reduced price of 12€, normally 15€.

To [pre-order your t-shirt](https://events.kde.org/), click the Pre-Order
T-shirt option in your Akademy registration and select the size you want.

**T-shirt pre-orders are open till the 17th of July, 23:59 UTC**

If you don't know whether or not you will be at Akademy, or if you don't want to
pre-order, there will be a limited number of t-shirts available to buy at the
event for 15€.

The fitted style is suitable for some women, while others may prefer to wear the
unisex style.

This years t-shirts are a slightly different shape to [previous
years](/2015/tshrt) but are
roughly the same size.

The approximate sizes of the t-shirts are:

**Unisex**

![Image of tshirt and table with dimensions of sizes](/media/2018/TU03T.jpg)

*4XL and 5XL are not available in the same colour unfortunately, if you order one
of these we will contact you to discuss this*

**Fitted**

![Image of tshirt and table with dimensions of sizes](/media/2018/TW04T.jpg)

*3XL is not available in the same colour unfortunately, if you order one of these
we will contact you to discuss this*

