---
title: Accommodation
menu:
  "2018":
    weight: 31
    parent: travel
---

## A & O Wien Hauptbahnhof

This year's recommended accommodation is [A&O Wien
Hauptbahnhof](https://www.aohostels.com/en/vienna/wien-hauptbahnhof/).

A & O provides

- Wi-Fi
- Small guest Kitchen
- Common rooms
- Billiards
- Table soccer
- ATM
- and even more, see [their
  website](https://www.aohostels.com/en/vienna/wien-hauptbahnhof/)

## How to book

A&O offers Single rooms, Twin rooms, 4-Bed Dormitory rooms & 6-Bed Dormitory
rooms.

Book your rooms individually at the [A & O
website](https://www.aohostels.com/en/vienna/wien-hauptbahnhof/). There are
different rates available (non-/cancelable, with/without breakfast) so choose
whichever fits you best. To get rooms near other Akademy attendees add "KDE
Konferenz" or "KDE Akademy" in the remarks section. While we can't promise
anything, we try to tell A&O to reserve rooms close to each other.

Vienna can get busy in August, so it is advised to book your accommodation
early.

## Location

Walking to A&O will take you approximately 5 to 10 minutes from Wien
Hauptbahnhof.

For information about how to get to the venue see the venue page.

**Address**  A&O Wien Hauptbahnhof  Sonnwendgasse 11  1100 Wien  Austria  

![Map showing location of A&O Wien Hauptbanhof](/media/2018/map_a&o_0.png)
<figcaption>Source: OpenStreetMap (© OpenStreetMap contributors, CC
BY-SA)</figcaption>

## Note for car travellers

Even though A & O provides parking space for guests parking lots in Vienna are
severly restricted. Public transport is recommended for rides from the
accomodation location to the city and back.

## Alternatives

If A & O does not fit your needs, we also recommend any other hostel/hotel in
the vicinity of Wien Hauptbahnhof.

## Sponsored accommodation

**People requesting accommodation sponsorship should not book their own
accommodation.**
