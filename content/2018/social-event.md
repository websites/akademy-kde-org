---
title: Social Event
menu:
  "2018":
    weight: 27
    parent: details
---


Sunday evening, after the talk part of Akademy ended, we will get together for
drinks, food & music. There will be an open buffet of turkish food (vegan &
vegetarian option available) and you will also get a few sponsored drinks both
alcoholic & non-alcoholic available.

## When & where

Sunday 12th of August, 8pm until 1am  
Cafe Derwisch - Partycellar  
next to U6 station "Thaliastraße"  

![Image with map of the venue and closest subway
station](/media/2018/map_derwisch.png)
<figcaption>CC OpenStreetMap</figcaption>

## How to get back to A&O?  ### Until the last subway

- Last U6 leaves at 00:28 from Thaliastraße
- U6 in direction Siebenhirten to Gumpendorfer Straße
- Tramway 18 in direction Schlachthausgasse until Hauptbahnhof
- Total trip time: Around 25 Minutes

### After the last subway

- Nightline N46 (runs every half hour at :04 & :34) from Thaliastraße to Oper,
  Karlsplatz (last stop)
- Nightline N66 (runs every half hour at :02 & :32) in direction Liesing from
  Oper, Karlsplatz to Hauptbahnhof
- Total trip time: Around 40 Minutes

