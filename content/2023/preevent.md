---
title: "Pre-event: Making a Difference"
menu:
  "2023":
    parent: program
    weight: 98 
---
## How to contribute and jump start your career in Free Software with the KDE Community

**The event has ended. You can [view the slides](/media/2023/preevent_presentation.pdf) and [recording](https://tube.kockatoo.org/w/n1PJguxBoP18m6VTJCNfnk).**

Join us for a hybrid event, either online or in-person at the University of Macedonia in Greece.

A workshop on how you can make a difference in the world of free software by getting involved with the KDE Community. Learn about our vision and community structure, and discover our impact on today's world. You will also hear from our community members on skill development, career growth, volunteering, and personal growth.

**Speakers:** Nate Graham and Neofytos Kolokotronis

**Date:** 23 May 2023

**Time:** 16:00 to 18:00 EEST

**Location:** Teleconference Room, University of Macedonia, 156 Egnatia str., PC 54636, Thessaloniki, Greece and online

**Topics:**

* What is KDE?
* How KDE has shaped today’s world?
* KDE’s continuing impact today
* How we got our start in KDE
* What can KDE can do for you?
* Akademy

This workshop is free to attend, however you need to register to reserve your space!
