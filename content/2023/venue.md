---
title: Venue
menu:
  "2023":
    parent: details
    weight: 4
---

Akademy 2023 is hosted at the University of Macedonia.

**Address:** University of Macedonia, 156 Egnatia str., PC 54636, Thessaloniki, Greece  
[**OpenStreetMap**](https://www.openstreetmap.org/way/14791176) - [**Google maps**](https://goo.gl/maps/UshfirmHXdogD3i7A)

**Website:** <https://www.uom.gr/en/contact>

**Bus lines to/from the venue:**
The University of Macedonia is conveniently located in the city centre, with the following bus stops in 2’ min walking distance:
* Ag. Fotini bus stop (C’ September str, near the University ). Lines 10, 31, 01X
* University of Macedonia bus stop (in front of the University entrance, Egnatia str.): Lines 02K, 07, 14, 58
* University of Macedonia bus stop (across the University, Egnatia str): Lines 01X, 02K, 14

**Bus information:** <https://oasth.gr/en>
