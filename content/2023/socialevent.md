---
title: Social event - Sunday
menu:
  "2023":
    parent: program
    weight: 3
---

**Venue:** [Klio Boat Party](https://kliocruise.gr/en/) <br>
**Date:** Sunday, 16 July 2023 <br>
**Time:** 21:45 - 00:00 <br>

This year, our social event will take place at Klio Floating Bar, one of the most iconic boats in Thessaloniki. Located right next to the White Tower, Klio offers a unique cruise on the beautiful waters of the Thermaikos Gulf. Enjoy a picturesque view of the harbor while sipping on a delicious homemade cocktail and indulging in mouth-watering finger foods in a cozy environment that will transport you straight to the Caribbean. As you enjoy the beautiful view of the sea, you will be serenaded by the sounds of Ethnic, Latin, Jazz, Reggae, and Roots music.

**Please note that the boat has limited capacity and can accommodate a maximum of 100 people. To ensure your spot on board, make sure to register in advance.**

Entrance is free and enjoy a complimentary drink of your choice - cocktail, wine, beer, or refreshment - along with delicious finger food. You can purchase additional drinks too.

Menu: mini chicken tortigias, mini sandwich with ham/cheese, mini sandwich tomato/feta cheese, wraps cold crepes or mini vegetable sandwitches, and pastries.

<a href="https://conf.kde.org/event/5/registrations/20/" class="button">Register</a>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Event",
  "endDate": "2023-07-17T00:00:00+03:00",
  "location": {
    "@type": "Place",
    "address": {
      "@type": "PostalAddress",
      "addressCountry": "GR",
      "addressLocality": "Θεσσαλονίκη",
      "postalCode": "546 21"
    },
    "geo": {
      "@type": "GeoCoordinates",
      "latitude": 40.6251494,
      "longitude": 22.9479387
    },
    "name": "Klio Floating Bar"
  },
  "name": "KDE Akademy 2023 Social Event",
  "startDate": "2023-07-16T21:45:00+03:00",
  "url": "https://akademy.kde.org/2023/socialevent/"
}
</script>
