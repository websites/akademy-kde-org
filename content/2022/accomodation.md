---
title: Accomodation
menu:
  "2022":
    parent: travel
    weight: 32
---

Barcelona is a relatively expensive place to stay, you should book as soon as
possible since our understanding is prices will only rise.

The [venue location]({{< ref "/" >}}venue) is relatively well connected by public transport to most of
the city, it should not be hard to find an hotel in ~30 minutes distance from
the venue.

## This is the hotel the sponsored attendees are going to be staying
ILUNION Les Corts Spa

25 minutes on foot to the venue (20 minutes if you take the tramway)

Please do not mention KDE when booking, it will only create confusion with the
hotel.  

## Other hotels in the area in relative walking distance of the venue

### Arenas Atiram Hotels

15 minutes on foot to the venue (really close to Bonanova Park Hotel)

A small number of sponsored attendees are staying in this hotel.

### Bonanova Park Hotel

15 minutes on foot to the venue (really close to Arenas Atiram Hotels)

### Hotel Catalonia Castellnou

25 minutes on foot to the venue (in the same general direction of Arenas Atiram
and Bonanova Park)

### Cheaper hotel in metro distance

The area is a bit empty due to do being in the convention center vicinity,
double check that it makes sense for you.

### easyHotel Barcelona Fira

30 minutes on metro to the venue Hostels

For those in a tighter budget there are quite a few hostels with shared bedrooms
for a much cheaper price (150-200€ the whole week).

