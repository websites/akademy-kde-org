---
title: Program
menu:
  "2022":
    weight: 20
    parent: details
---

<section id="block-system-main" class="my-3 block block-system clearfix">
  <article id="node-256" class="node node-page clearfix" about="/2022/program" typeof="foaf:Document">
    <div class="markdown-output"><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded"><p></p><div class="row py-5">
<div class="col-lg-10 mx-auto">
<div class="card rounded shadow border-0">
<div class="card-body p-5 rounded">
<div class="table-responsive">
<!--table border=1 style="border-top-style: solid; border-top-color: #ccc; border-top-width: 2px;"--><!--table id="program" style="width:100%" class="table table-striped table-bordered"--><table id="program" class="table table-striped  table-bordered">
<tr>
<th>Day</th>
<th>Morning</th>
<th>Afternoon</th>
<th>Evening</th>
</tr>
<tr>
<td>Thu 29th</td>
<td colspan="2"><center><a href="https://kde-espana.org/akademy-es-2022">Akademy-ES</a></center></td>
<td> </td>
</tr>
<td>Fri 30th</td>
<td colspan="2"><center><a href="https://kde-espana.org/akademy-es-2022">Akademy-ES</a><center></center></center></td>
<td><center><a href="https://akademy.kde.org/2022/welcome-event-friday">Welcome Event</a></center></td>

<tr>
<td>Sat 1st</td>
<td colspan="2"><center><a href="https://conf.kde.org/event/4/timetable/#20221001">Talks Day 1</a></center></td>
<td> </td>
</tr>
<tr>
<td>Sun 2nd</td>
<td colspan="2"><center><a href="https://conf.kde.org/event/4/timetable/#20221002">Talks Day 2</a></center></td>
<td><center><a href="https://akademy.kde.org/2022/social-event-sunday">Social Event</a></center></td>
</tr>
<tr>
<td>Mon 3rd</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2022/Monday">BoFs, Workshops &amp; Meetings</a><br /><a href="https://ev.kde.org/generalassembly/">KDE eV AGM</a></center></td>
<td><a href="https://conf.kde.org/event/4/registrations/15/">Barcelona Old City visit with guide</a></td>
</tr>
<tr>
<td>Tue 4th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2022/Tuesday">BoFs, Workshops &amp; Meetings</a></center></td>
<td></td>
</tr>
<tr>
<td>Wed 5th</td>
<td><center><a href="https://community.kde.org/Akademy/2022/Wednesday">BoFs, Workshops &amp; Meetings</a></center></td>
<td colspan="2"><center><a href="https://akademy.kde.org/2022/daytrip-wednesday">Daytrip</a></center></td>
</tr>
<tr>
<td>Thu 6th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2022/Thursday">Trainings and BoFs, Workshops &amp; Meetings</a></center></td>
<td> </td>
</tr>
<tr>
<td>Fri 7th</td>
<td colspan="2"><center><a href="https://community.kde.org/Akademy/2022/Friday">SuperComputer Visit and BoFs, Workshops &amp; Meetings</a></center></td>
<td> </td>
</tr>
</table>
</div>
</div>
</div>
</div>
</div>

</div></div></div></div>  <hr /></article>
</section>

