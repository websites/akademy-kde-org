---
title: Sponsors
menu:
  "2022":
    weight: 4
---

## Sponsorship Opportunities

**A big thank you to our sponsors who help to make this event happen! There are still sponsorship opportunities.
If you are interested, please see <a href="/2022/sponsoring">Sponsoring Akademy 2022</a> for more information, including valuable sponsor benefits.**

## Sponsors for Akademy 2022

### Gold

{{< sponsor homepage="https://shells.com" img="/media/2021/shells.png" >}}

<p id="shells">At <strong>Shells</strong> we are 
closing the digital divide by providing simple, secure, and affordable 
access to a powerful, future-proof computing solution that is accessible
 on any device.  In addition to providing a powerful computing option, 
we also offer a variety of pre-installed Linux distributions which give 
our users complete flexibility and control of their device while also 
opening new doors for a potential new Linux user.</p>

<https://shells.com>

{{< /sponsor >}}

{{< sponsor homepage="https://www.kdab.com" img="/media/2020/kdab3.png" >}}

<p id="kdab"></a><strong>KDAB</strong> is the world's 
leading software consultancy for architecture, development and design of
 Qt, C++ and OpenGL applications across desktop, embedded and mobile 
platforms. The biggest independent contributor to Qt, KDAB experts build
 run-times, mix native and web technologies, solve hardware stack 
performance issues and porting problems for hundreds of customers, many 
among the Fortune 500. KDAB’s tools and extensive experience in 
creating, debugging, profiling and porting complex, great looking 
applications help developers worldwide to deliver successful projects. 
KDAB’s global experts, all full-time developers, provide market leading 
training with hands-on exercises for Qt, OpenGL and modern C++ in 
multiple languages.</p>

<https://www.kdab.com>

{{< /sponsor >}}

### Silver

{{< sponsor homepage="https://www.canonical.com" img="/media/2022/ubuntu.png" >}}

<p id="canonical"><strong>Canonical</strong> is
 the publisher of Ubuntu, the OS for most public cloud workloads as well
 as the emerging categories of smart gateways, self-driving cars and 
advanced robots. Canonical provides enterprise security, support and 
services to commercial users of Ubuntu. Established in 2004, Canonical 
is a privately held company.</p>

<https://www.canonical.com>

{{< /sponsor >}}

{{< sponsor homepage="https://mbition.io" img="/media/2020/mbition_1.png" >}}

<p id="mbition">At <strong>MBition</strong>, we 
are bringing digital luxury to mobility users around the world. To 
accomplish this, we are redefining how software is developed within the 
automotive field as part of an international Mercedes-Benz software 
development network. As a 100% subsidiary of Mercedes-Benz AG, we 
develop and integrate the next generation of Mercedes-Benz in-car 
infotainment systems that are based on the Mercedes-Benz Operating 
System (MB.OS). But our contribution to MB.OS, the future of car 
software at Mercedes-Benz, does not stop here. To provide the most 
seamless &amp; connected mobility experiences, we further engage in 
Advanced Driver Assistance Systems (ADAS) platform development and 
continuously improve our Mercedes me companion app for our customers.
</p>

<https://mbition.io>

{{< /sponsor >}}

{{< sponsor homepage="https://www.qt.io" img="/media/2021/tqtc.png" >}}

<p id="tqc"><strong>The Qt Company</strong> is 
responsible for Qt development, productization and licensing under 
commercial and open-source licenses. Qt is a C++ based framework of 
libraries and tools that enables the development of powerful, 
interactive and cross-platform applications and devices. Used by over a 
million developers worldwide, Qt’s support for multiple desktop, 
embedded and mobile operating systems allows users to save significant 
time related to application and device development by simply reusing one
 code. Qt and KDE have a long history together, something The Qt Company
 values and appreciates. Code less. Create more. Deploy everywhere.</p>

<https://www.qt.io> and <https://qt-project.org>

{{< /sponsor >}}

{{< sponsor homepage="https://getfedora.org" img="/media/2021/fedora2021.png" >}}

<td><a name="fedora" id="fedora"></a><strong>The Fedora Project</strong>
 is a community of people who create an innovative, free, and open 
source platform for hardware, clouds, and containers that enables 
software developers and community members to build tailored solutions 
for their users.<br>The KDE Special Interest Group (SIG) is a group of 
Fedora contributors that maintain popular KDE packages, take care of 
KDE-related documentation, artwork and other KDE specific tasks.<br>

<https://getfedora.org> and <https://fedoraloveskde.org/>

{{< /sponsor >}}

### Bronze

{{< sponsor homepage="https://collabora.com" img="/media/2022/collabora2022.png" >}}

<p id="collabora"><strong>Collabora</strong> is
 a leading global consultancy specializing in delivering the benefits of
 Open Source software to the commercial world. For over 15 years, we've 
helped clients navigate the ever-evolving world of Open Source, enabling
 them to develop the best solutions – whether writing a line of code or 
shaping a longer-term strategic software development plan. By harnessing
 the potential of community-driven Open Source projects, and re-using 
existing components, we help our clients reduce time to market and focus
 on creating product differentiation.<br>

<https://collabora.com>

{{< /sponsor >}}

{{< sponsor homepage="https://www.opensuse.org" img="/media/2015/opensuse.png" >}}

<p id="opensuse"><strong>The openSUSE project</strong>
 is a worldwide effort that promotes the use of Linux everywhere. 
openSUSE creates one of the world's best Linux distributions, working 
together in an open, transparent and friendly manner as part of the 
worldwide Free and Open Source Software community. The project is 
controlled by its community and relies on the contributions of 
individuals, working as testers, writers, translators, usability 
experts, artists and ambassadors or developers. The project embraces a 
wide variety of technology, people with different levels of expertise, 
speaking different languages and having different cultural backgrounds.</p>

<https://www.opensuse.org>

{{< /sponsor >}}

{{< sponsor homepage="https://www.vikingsoftware.com" img="/media/2022/vikingsoftware.png" >}}

<p id="vikingsoftware"></a><strong>Viking Software</strong>
 is a leading consulting company that specializes in developing 
software, mainly Qt related. With our main office based in Denmark, we 
develop software solutions for customers in all of Europe and our 
services include – but are not limited to - embedded systems, Desktop 
Applications and Web systems.</p>
<p>
Working for Viking Software gives you the opportunity to be part of a 
team of highly skilled software engineers, while working on a variety of
 exciting projects with different customers. As part of the Viking Team,
 you maintain full support from the company, while being able to do what
 you do best. Viking Software cares about our employees and for us the 
key to quality software is found in ensuring our team members have a 
healthy work environment with a supportive team and where each employee 
is valued.</p>

<https://www.vikingsoftware.com>

{{< /sponsor >}}

{{< sponsor homepage="https://kde.slimbook.es" img="/media/2022/slimbook2022.png" >}}

<p id="slimbook"></a><strong>SLIMBOOK</strong> has 
been in the computer manufacturing business since 2015, we build 
computers tailored for Linux environments and ship them worldwide. 
Delivering quality hardware with our own apps combined with an 
unrivalled tech support team to improve the end user experience is our 
main goal. We also firmly believe that not everything is about the 
hardware, SLIMBOOK has been involved from the beginning with the 
community. We started taking on small local projects aiming to bring the
 GNU/Linux ecosystem to everyone, partnering with state of the art Linux
 desktops like KDE among other OS’s. And of course having our very own 
Linux academy, “Linux Center”, where free Linux/FOSS courses are 
imparted regularly for everyone. If you love Linux and need quality 
hardware to match, BE ONE OF US.</p>

<https://kde.slimbook.es/>

{{< /sponsor >}}

{{< sponsor homepage="https://www.codethink.co.uk" img="/media/2018/codethink.png" >}}

<p id="codethink"><strong>Codethink</strong> specialises in system-level
Open Source software
infrastructure to support advanced technical applications, working 
across a range of industries including finance, automotive, medical, 
telecoms. Typically we get involved in software architecture, design, 
development, integration, debugging and improvement on the deep scary 
plumbing code that makes most folks run away screaming.</p>

<https://www.codethink.co.uk>

{{< /sponsor >}}

### Supporter

{{< sponsor homepage="https://www.syslinbit.com" img="/media/2022/syslinbit.png" >}}

<p id="syslinbit"><strong>Syslinbit</strong> is
 helping companies big and small in using open source and free software 
successfully in their projects. Focusing on the embedded domain, we 
offer consulting services,  project coaching and trainings.</p>

<https://www.syslinbit.com>

{{< /sponsor >}}

{{< sponsor homepage="https://bcnfs.org" img="/media/2022/gitlab2022.png" >}}

<p id="gitlab"><strong>GitLab</strong> is The One 
DevOps platform for software innovation. As The One DevOps Platform, 
GitLab provides one interface, one data store, one permissions model, 
one value stream, one set of reports, one spot to secure your code, one 
location to deploy to any cloud, and one place for everyone to 
contribute. The platform is the only true cloud-agnostic end-to-end 
DevOps platform that brings together all DevOps capabilities in one 
place.</p>
<p>
With GitLab, organizations can create, deliver, and manage code quickly 
and continuously to translate business vision into reality. GitLab 
empowers customers and users to innovate faster, scale more easily, and 
serve and retain customers more effectively. Built on Open Source, 
GitLab works alongside its growing community, which is composed of 
thousands of developers and millions of users, to continuously deliver 
new DevOps innovations.</p>

<https://www.gitlab.com>

{{< /sponsor >}}

{{< sponsor homepage="https://pine64.org" img="/media/2021/pine2021.png" >}}

<p id="pine64"><strong>PINE64</strong> is a 
community driven project that creates Arm and RISC-V devices for open 
source enthusiasts and industry partners. Perhaps best known for the 
PinePhone, our Linux-only smartphone, and the Pinebook range of laptops,
 we strive to deliver devices that you want to use and develop for. 
Rather than applying business to a FOSS setting, we allow FOSS 
principles to guide our business.</p>

<https://pine64.org>

{{< /sponsor >}}

### Media Partners

{{< sponsor homepage="https://www.fosslife.org/newsletter" img="/media/2022/fosslife.png" >}}

<p id="fosslife"><strong>FOSSlife</strong> is 
dedicated to the world of free and open source software, focusing on 
careers, skills, and resources to help you build your future with FOSS. 
We provide timely information, useful insight, and practical resources 
for those who want to build or advance their career with open source. 
Subscribe to our weekly newsletter to get the latest from FOSSlife.</p>

<https://www.fosslife.org/newsletter>

{{< /sponsor >}}

{{< sponsor homepage="https://bit.ly/Linux-Update" img="/media/2018/linuxmagazine.png" >}}

<p id="linuxmagazine"><strong>Linux Magazine</strong>
 is your guide to the world of Linux and open source. Each monthly issue
 includes advanced technical information you won't find anywhere else 
including tutorials, in-depth articles on trending topics, 
troubleshooting and optimization tips, and more! Subscribe to our free 
newsletters and get news and articles in your inbox.</p>

<https://bit.ly/Linux-Update>

{{< /sponsor >}}

### Hosted by

{{< sponsor homepage="https://bcnfs.org" img="/media/2022/bcnfs.png" >}}

<p id="bcnfs"><strong>Barcelona Free Software</strong></p>
<https://bcnfs.org>

{{< /sponsor >}}

### Patrons of KDE
[KDE's Patrons](http://ev.kde.org/supporting-members.php) also support the KDE Community through out the year.
