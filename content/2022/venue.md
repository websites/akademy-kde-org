---
title: Venue
menu:
  "2022":
    weight: 21
    parent: details
---

The event will be held in Vertex building (41.3905, 2.1143) at Universitat
Politècnica de Catalunya (UPC):

Vertex building -
[OpenStreetMap](https://maps.upc.edu/?is=vertex&ic=10&il=cat&=vertex) or [Google
Maps](https://goo.gl/maps/43Woi7veoxNk7FLM6)

Plaça Eusebi Güell, 6

08034 Barcelona

Close [public transport
stations](https://akademy.kde.org/2022/getting-around-barcelona):

    Metro: L3 - Palau Reial station
    Tram: T1, T2, T3 - Palau Reial stop
    Bus: H6, 7, 33, 67, 113, 175 - Palau Reial stop
    Bus: V5, 63, 78  - Av Pedralbes Toquio / Av Pedralbes Pg Manuel Girona stop

From Palau Reial station, there is a walking distance of 850 m (about 13
minutes) to Vertex building.


