---
title: Daytrip - Wednesday
menu:
  "2022":
    weight: 24
    parent: details
---


## DAY TRIP - Wed Oct 5

![Image of Montserrat massif](/media/2022/ian-kelsall-JH2DwJUjjR0-unsplash.jpg)
This years day trip will be to Montserrat massif after the mornings BoFs.

[Register for Day Trip](https://conf.kde.org/event/4/registrations/12/)

Buses (free if you register for the day trip here will depart from [Avinguda
Diagonal with Carrer de John Maynard
Keynes](https://www.openstreetmap.org/?mlat=41.38600&mlon=2.11679#map=19/41.38600/2.11679) at 12:00 sharp (please be there at
11:50, it's a 10 minutes walk from the venue). It will be a black bus with
"Almeda" written on it. The bus will drop you off at the parking lot of the
Montserrat Abbey (~45 minutes of bus trip). It is recommended you get some
sandwich or similar with you for lunch and of course some water :).

There are multiple things to do: 

### Walk in the park

Please wear appropriate gear for what's a "relatively simple" hike.  Take the
[Sant Joan
Funicular](https://turistren.cat/en/trains/montserrat-rack-railway-and-funiculars/funicular-de-sant-joan/) - Cable car (~10€) from the Abbey area up to the Pla de les
Taràntules.  From there there's various routes one can take, all of them very
nice and mostly flat.  We can help with the funicular tickets cost if needed.
Speak to Kenny D (in person, seaLne on irc or @kenny:kde.org on matrix) 

#### Long Route (Rec'd)

The official route of the Natural Park is between 2.5 hours to 4 hours including
stops.

We will be going uphill in the Sant Jeroni mountain. It's the highest peak of
the massif (don't worry it's an easy hike).

[Learn
More](https://muntanyamontserrat.gencat.cat/web/.content/temes/el_parc/itineraris/arxius/SantJeroni/MontserratSantJeroni.pdf) 

#### Medium Route (Rec'd)

About 2.5km, no uphill elevation, the steps back to the abbey area are almost
250m downhill.

Walk from the Pla de les Taràntules (cable car upper station) to the Mirador de
la Serra de les Paparres and then walk down the the abbey area by the Pas dels
Francesos.

[Pla de les Taràntules to the Mirador de la Serra de les
Paparres](https://www.openstreetmap.org/directions?engine=graphhopper_foot&route=41.58875%2C1.83328%3B41.59233%2C1.82342)


[Mirador de la Serra de les Paparres to the abbey area by the Pas dels
Francesos](https://www.openstreetmap.org/directions?engine=graphhopper_foot&route=41.59266%2C1.82344%3B41.59193%2C1.83475#map=17/41.59198/1.83049)

#### Short Route (Rec'd)

About 2.5 km roundtrip, almost no elevation

Walk from the Pla de les Taràntules (cable car upper station) to the Mirador de
la Serra de les Paparres and back to the Pla de les Taràntules and take the
Funicular down (ticket is ~15 € roundrip instead of the ~10€ one way).

[Learn
More](https://www.openstreetmap.org/directions?engine=graphhopper_foot&route=41.58875%2C1.83328%3B41.59233%2C1.82342)

### Visit to the museum of Monstserrat
![Logo for Montserrat Museum](/media/2022/MM.svg)
The [Museum](https://en.wikipedia.org/wiki/Museum_of_Montserrat) (8€) holds more than 1300 pieces from masters like Caravaggio, Monet, Dalí, Picasso, etc.

### visit the abbey
![Image of Montserrat Abbey](/media/2022/w-k-ZJKWzMowHUo-unsplash.jpg)
The Montserrat Abbey is one of Catalonia's most important religious buildings
holding ["La Moreneta"](https://en.wikipedia.org/wiki/Virgin_of_Montserrat), the Saint Patron of Catalonia.

### ... and chill

The abbey area has a few bars with nice views of the surrounding mountains, just
sit and have a drink, October should hopefully have a [not too cold, not too
hot] temperature to be enjoying some time out in the nature.


## the return

The buses will return at 19:00 from the parking lot of the Montserrat Abbey.
