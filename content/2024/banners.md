---
title: I'm going to Akademy banners
menu:
  "2024":
    parent: details
    weight: 8
hideSponsors: false
---
Use these banners to promote Akademy on your websites, blogs, social media headers, or any other platform.

There are three different sizes to choose from. If you decide to use the banners, it would be great if you could link them to the Akademy website to help attract more visitors.

Let's spread the word about Akademy and make it a success!

![600x110](https://akademy.kde.org/media/2024/banners/akademy2024-banner-600x110.png)

![728x90](https://akademy.kde.org/media/2024/banners/akademy2024-banner-728x90.png)

![160x600](https://akademy.kde.org/media/2024/banners/akademy2024-banner-160x600.png)

(These banners were created by Andy B.)

