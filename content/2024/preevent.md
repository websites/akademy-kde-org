---
title: "Pre-event: Making a Difference"
menu:
  "2024":
    parent: details
    weight: 11 
---
## How to contribute and jump start your career in Free Software with the KDE Community

This is a workshop on how you can make a difference in the world of free software by
getting involved with the KDE Community. Learn about our vision and community structure
and discover our impact on today's world.  You will also hear from our community members
on skill development, career growth, volunteering, and personal growth.

If you're already involved in KDE this is a perfect event to invite your KDE curious
friends too!

**Speaker:** Aleix Pol

**Date:**  Saturday, 10 August 2024

**Time:** 16.00 CEST

**The event has ended. You can [view the recording](https://tube.kockatoo.org/w/op6JnEknfotW9QnQFginJp).**

