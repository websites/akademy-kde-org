---
title: Day trip - Wednesday
menu:
  "2024":
    parent: details
    weight: 7
---

**Place:** Rothenburg ob der Tauber <br>
**Date:** Wednesday, 11 September 2024 <br>
**Time:** 0900 <br>

Our day trip is to Rothenburg ob der Tauber, one of Germany’s best-preserved medieval towns. The bus will drop us of outside of the town, from where we will take a 15-20 minute walk to the town centre and explore the cobbled streets with half-timbered houses. At 11:00, we have a guided tour (roughly 90 minutes). The tour starts at the fountain on the market square. After the tour, you can explore the town on your own. One highlight is climbing the town hall tower, which, although challenging with its narrow steps and ladder at the top, offers a stunning view of the town for a fee of 2.50€, payable at the top. Please note, that only 20 people can be on the final set of stairs and the narrow walkway at the top simultaneously.

Beyond the tower, Rothenburg boasts several museums with historical artefacts, a castle garden, Tauber Valley and a walk around the town wall.

If you plan to eat in Rothenburg, please make a reservation, as restaurants do not entertain guests without one. However, there is one doner shop that offers food without a reservation, so you can go there if needed. Don't miss trying a Schneeball pastry while you're there.

<strong>Plan for the day:</strong>

- 09.00 Departure from the central station. Please ensure you are there a bit early as we want to leave as on time

- 10.00 Arrival at Rothenburg ob der Tauber

- 11.00 Guided tour

- 17.00 Departure from Rothenburg ob der Tauber

- 18.00 Arrival in Würzburg

See [openstreetmap](https://www.openstreetmap.org/#map=19/49.80134/9.93315) or the map below for the exact position. When standing in front of the station building, go past the bus stops and to the left of the tall building. Our bus departs across the street (Bismarckstraße) from this building.

![Departure station of the Bus](/media/2024/bus_station.png)


The above timetable might slightly vary, departure time or any other normal delays.

<strong>Notes:</strong>
* You need to have registered for the daytrip to attend and have your badge with you
* The museum entrance fee and any food/drinks you want need to be purchased yourself. We want everyone to be able to take part in the Daytrip and money should not be the reason you miss out. If the entrance fee is too expensive for you due to being on a low income please speak to Kenny D and we can cover that for you.
* Ensure you have water bottles and any snacks you might want
* Participants are required to wear comfortable shoes

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Event",
  "location": {
    "@type": "Place",
    "address": {
      "@type": "PostalAddress",
      "addressCountry": "DE",
      "addressLocality": "Würzburg",
      "postalCode": "97070",
      "streetAddress": "Bismarckstraße"
    },
    "geo": {
      "@type": "GeoCoordinates",
      "latitude": 49.801340,
      "longitude": 9.933150
    },
    "name": "Würzburg central station"
  },
  "name": "Akademy 2024 Day Trip to Rothenburg ob der Tauber",
  "startDate": "2024-09-11T09:00:00+02:00",
  "endDate": "2024-09-11T18:00:00+02:00",
  "url": "https://akademy.kde.org/2024/daytrip/"
}
</script>
