---
title: Sponsors
menu:
  "2024":
    weight: 6
hideSponsors: true
---
## Sponsorship Opportunities

A big thank you to our sponsors who help to make this event happen! There are still sponsorship opportunities available.
If you are interested, please see [Sponsoring Akademy](/2024/sponsoring) for more information, including valuable sponsor benefits.

## Sponsors for Akademy 2024

### Platinum

{{< sponsor homepage="https://www.qt.io" img="/media/2024/qt-group-logo-light-mode.svg" img_dark="/media/2024/qt-group-logo-dark-mode.svg">}}

<p id="tqc"><strong>Qt Group</strong> is responsible for Qt development, productization and licensing under commercial and open-source licenses. Qt is a C++ based framework of libraries and tools that enables the development of powerful, interactive and cross-platform applications and devices. Used by over a million developers worldwide, Qt’s support for multiple desktop, embedded and mobile operating systems allows users to save significant time related to application and device development by simply reusing one code. Qt and KDE have a long history together, something Qt Group values and appreciates. Code less. Create more. Deploy everywhere.</p>

<https://www.qt.io> and <https://qt-project.org>

{{< /sponsor >}}

### Silver

{{< sponsor homepage="https://www.canonical.com" img="/media/2024/ubuntu-light-mode.svg" img_dark="/media/2024/ubuntu-dark-mode.svg" >}}

<p id="canonical"><strong>Canonical</strong> is the publisher of Ubuntu, the OS for most public cloud workloads as well as the emerging categories of smart gateways, self-driving cars and advanced robots. Canonical provides enterprise security, support and services to commercial users of Ubuntu. Established in 2004, Canonical is a privately held company.</p>

<https://www.canonical.com>

{{< /sponsor >}}

### Bronze

{{< sponsor homepage="https://www.opensuse.org" img="/media/2024/openSUSE.svg" >}}

<p id="suse"><strong>The openSUSE project</strong> is a worldwide effort that promotes the use of Linux everywhere. openSUSE creates one of the world's best Linux distributions, working together in an open, transparent and friendly manner as part of the worldwide Free and Open Source Software community. The project is controlled by its community and relies on the contributions of individuals, working as testers, writers, translators, usability experts, artists and ambassadors or developers. The project embraces a wide variety of technology, people with different levels of expertise, speaking different languages and having different cultural backgrounds.</p>

<https://www.opensuse.org>

{{< /sponsor >}}

{{< sponsor homepage="https://www.kdab.com/" img="/media/2024/kdabtrusted-light-mode.svg" img_dark="/media/2024/kdabtrusted-dark-mode.svg" >}}

Trusted software excellence across embedded and desktop platforms.

<p id="kdab">The <strong>KDAB</strong> Group is a globally recognized provider for software consulting, development and training, specializing in embedded devices and complex cross-platform desktop applications. In addition to being leading experts in Qt, C++ and 3D technologies for over two decades, KDAB provides deep expertise across the stack, including Linux, Rust and modern UI frameworks. With 100+ employees from 20 countries and offices in Sweden, Germany, USA, France and UK we serve clients around the world.

KDE and KDAB maintain a close, long-lasting relationship. As a testament to this bond, KDAB has been sponsoring KDE Akademy for many years.</p>

<https://www.kdab.com>

{{< /sponsor >}}

{{< sponsor homepage="https://kde.slimbook.es" img="/media/2024/slimbook-light-mode.svg" img_dark="/media/2024/slimbook-dark-mode.svg">}}

<p id="slimbook"><strong>SLIMBOOK</strong> has been in the computer manufacturing business since 2015. We build computers tailored for Linux environments and ship them worldwide. Our main goal is to deliver quality hardware with our own apps combined with an unrivaled tech support team to improve the end-user experience. We also firmly believe that not everything is about the hardware. SLIMBOOK has been involved with the community from the beginning, taking on small local projects to bring the GNU/Linux ecosystem to everyone. We have partnered with state-of-the-art Linux desktops like KDE, among other operating systems. And of course, we have our very own Linux academy, "Linux Center," where we regularly impart free Linux/FOSS courses for everyone. If you love Linux and need quality hardware to match, BE ONE OF US.</p>

<https://kde.slimbook.es>

{{< /sponsor >}}

### Supporter

{{< sponsor homepage="https://haute-couture.enioka.com/en" img="/media/2024/enioka-light-mode.svg" img_dark="/media/2024/enioka-dark-mode.svg" >}}

**enioka Haute Couture**: Businesses of all kinds have become highly dependent on their IT systems. Yet, many of them have lost control of this essential part of their assets. Enioka Haute Couture specializes in mastering complex software development, whether it's due to the domain, organization, or timing of the project. We work closely with your teams to build software tailored to your needs. If we're developing it ourselves, we go the extra mile to ensure you keep control of it. We also help your teams adopt the right organization, technologies, and tools. Finally, whenever appropriate, we can help you set up free and open-source software projects or interact with existing ones. With Enioka Haute Couture, you have a trusted provider who facilitates your development needs while keeping full control of your systems. No more vendor or service provider lock-in.

<https://haute-couture.enioka.com/en>

{{< /sponsor >}}

{{< sponsor homepage="https://extenly.com" img="/media/2024/extenly-light-mode.svg" img_dark="/media/2024/extenly-dark-mode.svg" >}}

Founded in 2019, **Extenly** has built a reputation for delivering successful products and ensuring customer satisfaction. As Qt experts, we specialize in embedded UI and cross-platform app development, providing top-quality, reusable code with optimal performance using open-source tools and frameworks. Our services include UI/UX design, Qt/QML UI development, rapid prototyping, consultancy, and Qt/QML training. We assist our clients in creating scalable UI applications that can be easily adapted and deployed across different systems and platforms, saving both time and money.

<https://extenly.com>

{{< /sponsor >}}

{{< sponsor homepage="https://www.codethink.co.uk" img="/media/2024/codethink.svg" >}}

**Codethink** specialises in system-level Open Source software infrastructure to support advanced technical applications, working across a range of industries including finance, automotive, medical, telecoms. Typically we get involved in software architecture, design, development, integration, debugging and improvement on the deep scary plumbing code that makes most folks run away screaming.

<https://www.codethink.co.uk>

{{< /sponsor >}}

{{< sponsor homepage="https://www.tuxedocomputers.com" img="/media/2024/tuxedo-light-mode.svg" img_dark="/media/2024/tuxedo-dark-mode.svg" >}}

**TUXEDO**: The name TUXEDO Computers unites both the demands and the product range: while it's name is a tailor-made suit, it also contains the name of the Linux mascot Tux. TUXEDO Computers are not only Linux hardware in a tailor-made suit, they are immediately recognizable as such by their name. Only where TUXEDO is written on it, there is Linux hardware in a tailor-made suit inside! They have been producing individual computers and laptops for nearly two decades now - all pre-installed with Linux! TUXEDO Computers offers their in house built, maintained and developed Linux distribution TUXEDO OS with KDE Plasma as its desktop environment. This offer is complemented by gratis technical support and services around Linux. TUXEDO Computers select all components suitable for the operation with Linux and assemble the devices in Germany. Afterwards all TUXEDOs are delivered in a way that you are ready to start immediately!

<https://www.tuxedocomputers.com>

{{< /sponsor >}}

### Media Partners

{{< sponsor homepage="https://bit.ly/Linux-Update" img="/media/2024/LinuxMagazineLogo.png" >}}

**Linux Magazine** is your guide to the world of Linux and open source. Each monthly issue includes advanced technical information you won't find anywhere else including tutorials, in-depth articles on trending topics, troubleshooting and optimization tips, and more! Subscribe to our free newsletters and get news and articles in your inbox.

https://bit.ly/Linux-Update

{{< /sponsor >}}

### KDE Patrons
[KDE Patrons](http://ev.kde.org/supporting-members.php) also support the KDE Community throughout the year.
