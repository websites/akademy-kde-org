---
title: Social Event - Sunday
menu:
  "2024":
    parent: details
    weight: 4
---

**Date:** Sunday, 8 September 2024

**Time:**  Starts at 20:00

**Place:** Waldschänke Dornheim, Talaveraplatz 97082 Würzburg

**How to get there:** Take the (rail replacement) bus to station "Talavera", see [openstreetmap](https://www.openstreetmap.org/node/4355268139)

**Website:** [https://waldschaenke-dornheim.de/](https://waldschaenke-dornheim.de/)

Join us for our social event at Waldschänke Dornheim on Sunday, September 8th, starting at 20:00. Enjoy the venue's outdoor garden and take the opportunity to connect with fellow attendees.

You'll receive drink tokens, and food will also be available.

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Event",
  "location": {
    "@type": "Place",
    "address": {
      "@type": "PostalAddress",
      "addressCountry": "DE",
      "addressLocality": "Würzburg",
      "postalCode": "97082",
      "streetAddress": "Talaveraplatz"
    },
    "geo": {
      "@type": "GeoCoordinates",
      "latitude": 49.7994523,
      "longitude": 9.9192246
    },
    "name": "Waldschänke Dornheim"
  },
  "name": "Akademy 2024 Social Event",
  "startDate": "2024-09-08T20:00:00+02:00",
  "url": "https://akademy.kde.org/2024/welcome-event/"
}
</script>
