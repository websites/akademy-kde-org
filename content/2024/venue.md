---
title: Venue
menu:
  "2024":
    parent: details
    weight: 3
hideSponsors: false
---

Akademy 2024 is hosted at the Julius-Maximilians-Unversity of Würzburg.

The Julius-Maximilians-Unversity of Würzburg is the fourth oldest university in Germany, being founded in 1402. Historically and to this day, it has a strong reputation in areas like medicine and law, physics, chemistry, and other sciences. 14 Nobel laureates have taught and researched at the University, including Wilhelm Conrad Röntgen, who was awarded the first ever Nobel Price in physics for having discovered X-Rays while working at the university.

To arrive at the venue multiple bus lines can be used. From the city centre take one of the lines 10, 14, 34, 114, or 214 and exit at stops "Hubland/Mensa", "Universitätszentrum", "Am Hubland" or "Mathematisches Institut".
Afterwards it is a short walk across the campus.

![Map showing how to walk from the bus station to the venue](/media/2024/venue.png)

**Address:** Julius-Maximilians-Unversity, Am Hubland , 97074 Würzburg, Germany

**Map:** <https://www.openstreetmap.org/way/24680861>

**Website:** <https://wueaddress.uni-wuerzburg.de/building/3515>
