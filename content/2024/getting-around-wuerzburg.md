---
title: Getting around Würzburg
menu:
  "2024":
    parent: travel
    weight: 3
hideSponsors: false
---

Puclic transport in Würzburg consists of trams and buses. Tickets can be bought
from machines or from the bus driver. Machines are generally available at every
tram stop and a few bus stops. Please note that you will need cash in order to buy
a ticket from the bus driver. Alternatively you can use [the FAIRTIQ app](https://www.wvv.de/mobil-b2c/fairtiq/) to buy tickets.
Compared to other public transport providers in Germany it's **not** possible to use the DB app to buy a ticket instead it
will you offer the much more expensive Bayernticket.

**Note**: Due to construction in the city centre, tram traffic is limited until the 9th of September but there are replacement buses available

## Tickets and fares

* A Deutschlandticket (see [Travel to Würzburg]({{< ref "/2024/travel-to-wuerzburg" >}})) is valid in every bus and tram
* A single fare costs 3.10€
* A ticket for 6 trips costs 12.70€
* A day ticket costs 5.20€
* A day ticket for two people ("Tageskarte Plus") costs 7.40€

Day tickets are valid on the day they are bought. When bought on Saturday they are also valid on Sunday.

The 6 trip ticket needs to be validated/stamped for each trip. There are machines in every bus and tram
stations for this.

## How to get to the Venue

See map and description at [Venue]({{< ref "/2024/venue" >}}).

## Other

There are also Taxis, bike rentals, and scooters available.


## Useful links

* [Map and schematic with available lines](https://netzplan.vvm-info.de/)
* [DB](https://int.bahn.de/en) and [MoBY](https://bahnland-bayern.de/de/moby/efa/app) to search for trips
* General info: [VVM](https://www.vvm-info.de/) and [WVV](https://www.wvv.de/mobil-b2c/oepnv/)
