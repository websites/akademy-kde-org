---
title: Things to Do in Würzburg
menu:
  "2024":
    parent: travel
    weight: 4
hideSponsors: false
---

Würzburg is home to some historic buildings that are worth visiting.

The Würzburger Residenz was built by the prince-bishops of Würzburg to live in, after leaving the castle. Together with the Hofgarten (the park behind the palace), it is a world heritage site widely known for containing one of the largest frescoes in the world. It is home to several museums and can be visited daily. There are daily guided tours in German and English.

The Festung is a castle on top of the Marienberg (pictured) and was the home of the prince-bishops before the Residenz was built. Today, it can be visited as a museum. There's a great view of the city from the entrance to the castle. Due to construction work, entrance to the castle might not always be possible.

The Alte Mainbrücke was built in the 15th century and is the oldest bridge over the river Main in Würzburg. Due to its stone figures, it is often compared to the Charles Bridge in Prague and the Ponte Sant'Angelo in Rome. Today, it is famous for the "Brückenschoppen" - drinking wine while standing on the bridge.

Würzburg also contains roughly 60 churches which can be visited. Due to its location next to a river in a hilly area, it's a great area for biking and hiking, both along the river for easy tours with a great scenery, and more advanced tours in the hills. In the north of Würzburg, the "Würzburger Stein" vineyard is great for having a walk while enjoying the view over the city.
