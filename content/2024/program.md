 ---
title: Program
menu:
  "2024":
    weight: 1
hideSponsors: false
---
<div class="table-responsive">
<!--table border=1 style="border-top-style: solid; border-top-color: #ccc; border-top-width: 2px;"-->
<!--table id="program" style="width:100%" class="table table-striped table-bordered"-->
<table id="program" class="table table-striped  table-bordered">
<tr>
<th>Day</th>
<th>Morning</th>
<th>Afternoon</th>
<th>Evening</th>
</tr>
<tr>
<td>Fri 6th</td>
<td colspan=2><center>&nbsp;<center></td>
<td><center><a href="https://akademy.kde.org/2024/welcome-event/">Welcome Event</a></center></td>
</tr>
<tr>
<td>Sat 7th</td>
<td colspan=2><center><a href="https://conf.kde.org/event/6/timetable/?layout=room#20240907">Talks Day 1</a></center></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Sun 8th</td>
<td colspan=2><center><a href="https://conf.kde.org/event/6/timetable/?layout=room#20240908">Talks Day 2</a></center></td>
<td><center><a href="https://akademy.kde.org/2024/social-event/">Social Event</a></center></td>
</tr>
<tr>
<td>Mon 9th</td>
<td colspan=2><center><a href="https://community.kde.org/Akademy/2024/Monday">BoFs, Training, Workshops & Meetings</a></center></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Tue 10th</td>
<td colspan=2><center><a href="https://community.kde.org/Akademy/2024/Tuesday">BoFs, Training, Workshops & Meetings</a></center></td>
<td></td>
</tr>
<tr>
<td>Wed 11th</td>
<!--td><center><a href="https://community.kde.org/Akademy/2023/Wednesday">BoFs, Workshops & Meetings</a></center><--/td-->
<td colspan=3><center><a href="https://akademy.kde.org/2024/daytrip/">Daytrip</a></center></td>
</tr>
<tr>
<td>Thu 12th</td>
<td colspan=2><center><a href="https://community.kde.org/Akademy/2024/Thursday">BoFs, Workshops & Meetings</a></center></td>
<td>&nbsp;</td>
</tr>
</table>
</div>

To interact with Akademy attendees online you must first [register](https://akademy.kde.org/2024/register/) (if you haven't already) and then join
- [the Akademy matrix space](https://go.kde.org/matrix/#/#attend-akademy:kde.org)
- [join Room 1 questions](https://go.kde.org/matrix/#/#akademy-talks1:kde.org)
- [join Room 2 questions](https://go.kde.org/matrix//#/#akademy-talks2:kde.org)

### Conference Streams

| Room 1 | Room 2 |
|--      |--      |
| [Youtube](https://www.youtube.com/watch?v=gTxRaBEUe-I) |  [Youtube](https://www.youtube.com/watch?v=bovuuPZxWC4) | 
| [Peertube]() | [Peertube]() Links added soon |

## Qt Contributor Summit

Right before Akademy on September 5th and 6th the Qt Project will come together in Würzburg at the [Qt Contributor Summit](https://wiki.qt.io/Qt_Contributor_Summit_2024). KDE contributors are welcome to join and help move Qt forward.
