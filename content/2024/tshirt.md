---
title: Akademy 2024 T-shirts
menu:
  "2024":
    parent: details
    weight: 9
---
Following on from previous years we have the option for buying an official Akademy 2024 T-shirt. As Akademy is hybrid we have two options depending on if you are attending in person or online.

<center>
<a href="/media/2024/akademy2024-mock-attendees-med.png">
  <img src="/media/2024/akademy2024-mock-attendees-med.png" style="width:100%"/>
</a>
<figcaption><center>
Mockup of Akademy 2024 T-shirt by Jens Reuterberg
&nbsp;
</center></figcaption>
</center>

This year's T-shirts are the same model as last years. We offer them in both Straight Cut and Fitted Cut. The approximate sizes of the T-shirts are:

#### Straight Cut

<div style="text-align: center;">

[![](/media/2023/unisex-sizes.png)](/media/2023/unisex-sizes.png)

*XXXXL and XXXXXL are available in a slightly different colour*

</div>

#### Fitted Cut

<div style="text-align: center;">

[![](/media/2023/fitted-sizes.png)](/media/2023/fitted-sizes.png)

*XXXL is available in a slightly different colour*

</div>

<div class="row">
  <div class="col-md-6">

### Würzburg: in person

In person attendees can pre-order T-shirts for collection in Würzburg for **15€** (paid when collecting in cash).

**Orders now closed**

<!--
<a href="https://conf.kde.org/event/6/registrations/27/" class="button">Pre-order your T-shirt</a>

  </div>
  <div class="col-md-6">
-->

### Online attendees

Online T-shirts cost **25€**, which includes worldwide delivery. To keep the price the same for everyone, [KDE e.V.](https://ev.kde.org) is subsidising the higher delivery cost for those outside Europe.

Orders for the online attendees are being handled directly by FreeWear, the same company who has been printing our Akademy T-shirts for years.

*The payment processing fees for KDE e.V. if you pay with credit-card are far cheaper than using PayPal.*


**Online orders are open till the 29th  September and will start to be shipped from the 10th of October.**

<a href="https://www.freewear.org/Akademy/" class="button">Buy your T-shirt for online attendees</a>

  </div>
</div>

*FreeWear also carry a range of other <a href="https://www.freewear.org/kde">KDE & FLOSS merchandise</a> which you can order separately online<!-- or we will have a selection available in Barcelona-->.*
