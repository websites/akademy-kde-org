---
title: Welcome Event - Friday
menu:
  "2024":
    parent: details
    weight: 4
---

**Date:** Friday, 6 September 2024

**Time:**  Starts at 19:30

**Place:** Wohnzimmer Bar Würzburg, Tiepolostr 21, 97070 Würzburg

**How to get there:** Take the bus/tram to station "Sanderring", see [openstreetmap](https://www.openstreetmap.org/#map=18/49.78744/9.93068)

**Website:** [https://wohnzimmer-bar.com/wuerzburg/](https://wohnzimmer-bar.com/wuerzburg/)

Meet fellow attendees in a relaxed bar to get ready for Akademy. You will be able to collect your name badge and get to know other attendees.

You will receive tokens for one snack and two drinks.

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Event",
  "location": {
    "@type": "Place",
    "address": {
      "@type": "PostalAddress",
      "addressCountry": "DE",
      "addressLocality": "Würzburg",
      "postalCode": "97070",
      "streetAddress": "Tiepolostr 21"
    },
    "geo": {
      "@type": "GeoCoordinates",
      "latitude": 49.7874429,
      "longitude": 9.9313562
    },
    "name": "Wohnzimmer Bar"
  },
  "name": "Akademy 2024 Welcome Event",
  "startDate": "2024-09-06T19:30:00+02:00",
  "url": "https://akademy.kde.org/2024/welcome-event/"
}
</script>
