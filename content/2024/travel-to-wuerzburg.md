---
title: Travel to Würzburg
menu:
  "2024":
    parent: travel
    weight: 1
hideSponsors: false
---

### National
In Germany, Würzburg is easily reachable by train, with direct connections to many large German cities. There are direct long-distance trains from/to Frankfurt, Cologne, Hamburg, Stuttgart, Hannover, Nürnberg, Erfurt, Munich, and many other cities. For others, one interchange is usually enough.

### International
From some cities in neighboring countries, travelling to Würzburg by train is a viable option. In other cases, flying into Frankfurt or Nürnberg is recommended. Since Frankfurt is one of the largest airports in Europe, it is well-reachable from most places in the world. There are direct long-distance trains from Frankfurt airport to Würzburg. There are also regional trains between Frankfurt Main Station and Würzburg. There are direct long distance and regional trains between Nürnberg Main Station and Würzburg. Nürnberg Main Station can easily be reached from the airport using the subway. Coming in from other German airports is also possible, but we recommend against doing so since it usually involves longer travel times in Germany.

### Going by train in Germany

Germany operates two classes of trains, local trains (e.g. RE, RB, HLB) and long-distance/high-speed (ICE, IC). Different tickets are needed for those types.

Tickets for high-speed trains should be purchased in advance on the [Deutsche Bahn website](https://bahn.de/en) or the DB Navigator app. There's two kinds of tickets available: 'Sparpreis' is cheaper but bound to a specific connection while 'Flexpreis' allows to take any connection during the day. Tickets are cheaper when booked early. ICE trains come with power sockets, Wifi, and a bistro. Seat reservations are optional and can be purchased for 5€.

For regional trains you can buy tickets online via the DB website or navigator app, or at local vending machines. There's also the 'Deutschlandticket', which allows you to take any regional transportation (local trains, busses, trams etc; but no ICE/IC). It's a subscription for 49€/month but can be cancelled immediately. Given you will likely use transportation in Würzburg this will be the most cost-effective option for many. It can be purchased at e.g. https://www.ticketswvv.de.

From Frankfurt Airport there are direct and frequent ICE connections to Würzburg that take 1h30m. Alternatively you can take regional trains (with one change at e.g. Frankfurt Hauptbahnhof) that take about 2h20m.

From Nürnberg airport take the U-Bahn (subway) to Nürnberg Hauptbahnhof. From there both ICE (50min) and local trains (1h10m) are available.

Tickets need to be shown on demand to the conductor on the train. For the Deutschlandticket you need some form of ID (passport, drivers license, etc).

In case of delay/disruption of over 20 minutes you are allowed to take any alternative train without purchasing another ticket. For assistance consult the conductor, station helpdesk, or the Akademy team.
